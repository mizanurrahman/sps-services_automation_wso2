package com.scholastic.facade.Automation_WS02;

import org.junit.Before;

import com.jayway.restassured.RestAssured;

import static com.jayway.restassured.config.RestAssuredConfig.config;
import static com.jayway.restassured.config.SSLConfig.sslConfig;
public class BaseTest
{
	@Before
	public void init() 
	{
		//RestAssured.baseURI = "http://int-api.scholastic-labs.com/qa1";
		//RestAssured.baseURI = "http://int-api.scholastic-labs.com";
		RestAssured.config = config().sslConfig(sslConfig().allowAllHostnames());
		RestAssured.baseURI = "https://nonprod.api.scholastic.com";
	//	RestAssured.port = 80;
		//RestAssured.basePath ="/app/sps-lookup/1.0";
		RestAssured.basePath ="/qa1";
		System.out.println("\n\n\n\n*******Initialized RestAssured*******");
		System.out.println("RestAssured.baseURI: " + RestAssured.baseURI);
		System.out.println("RestAssured.port: " + RestAssured.port);
		System.out.println("RestAssured.basePath: " + RestAssured.basePath); 
		System.out.println("******************************\n\n\n\n");
	}	
	
	public String getAccesToken()
	{
		return "QX507NurAlNremIGU6SjoeHPQ9oa";
	}

}


