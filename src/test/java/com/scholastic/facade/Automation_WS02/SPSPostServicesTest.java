package com.scholastic.facade.Automation_WS02;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.isEmptyString;

import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.Matchers.containsString;

import com.jayway.restassured.builder.ResponseSpecBuilder;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.ExtractableResponse;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.ResponseSpecification;
public class SPSPostServicesTest extends BaseTest 
{
	//Manual School
	private String accessToken;
	private String m_name ="Manual School Inc. 121";
	private String m_address1 ="557 Broadway";
	private String m_address2 ="";
	private String m_address3 ="";
	private String m_address4 ="";
	private String m_address5 ="";
	private String m_city ="NYC";
	private String m_state ="NY";
	private String m_zipcode ="10012";
	private String m_country ="";
	private String m_poBox ="";
	private String m_county ="";
	private String m_phone ="2123436111";
	private String m_reportingSchoolType ="PS";
	
	//Home School
	private String h_name ="Home School Inc. 1212";
	private String h_address1 ="578 Broadway";
	private String h_address2 ="";
	private String h_address3 ="";
	private String h_address4 ="";
	private String h_address5 ="";
	private String h_city ="NYC";
	private String h_state ="NY";
	private String h_zipcode ="10012";
	private String h_country ="";
	private String h_poBox ="";
	private String h_county ="";
	private String h_phone ="2123432266";
	private String h_reportingSchoolType ="HOS";
	
	//Login for Session Id and TSP Id
	//private String user ="27selfbtsuser37@scholastic.com";
	private String user ="qatestsample702@scholastic.com";
	//private String user ="27selfbtsuser70@scholastic.com";
	private String password ="passed1";
	private boolean singleToken = true;
	private boolean userForWS = false;
	private boolean addCookie = true;
	private boolean addExpiration = true;
	private String sps_session;
	private String sps_tsp;
	
	//Make Payment 
	private String cardType ="001";
	private String cardExpMonth ="12";		
	private String cardExpYear ="2016";
	private String firstName ="Mo";
	private String lastName ="Zo";
	//private String cardSecurityCode;		
	private String phone ="7185543378";
	private String cardNumber ="401200xxxxxx0026";
	private String addrOne ="34-34 70th Street";
	private String addrTwo ="sdfdsf";		
	private String zip ="11372";
	private String state ="NY";
	private String country ="US";
	private String email ="qatestsample702@scholastic.com";		
	private String chargeAmount ="4.21";
	private String city ="Jackson Heights";
	private String paymentToken ="0102234367111111";
	private String transactionId;
	//private String transactionId ="4363019505205000001520";
	private String authCode ="831000";
	//private String storeWalletFlag ="";	
	private String authTime ="2015-07-07T200912Z";
	private String reconcileId ="11905342";
	//private String bcoe="0012511077";
	private String bcoe="0012554549"; 
	private String userSPSID;
	

	private static final String ENDPOINT_CREATE_MANUALSCHOOL="/app/sps-organizations/1.0?clientId=NonSPSClient";
	private static final String ENDPOINT_CREATE_HOMESCHOOL="/app/sps-organizations/1.0?clientId=NonSPSClient";
	//private static final String ENDPOINT_BCOE_ORDERHISTORY="/app/bcoe-orderhistory/CustomerOrdersService";
	private static final String ENDPOINT_MAKE_PAYMENT="/user/bpbpsweb/MakePayment";
	//private static final String ENDPOINT_MAKE_PAYMENT="http://bpbps.qa1.scholastic.com/BPBPSWeb/MakePayment";
	
	
	
	@Test
	public void createManualSchoolTest()
	{	
		System.out.println("########################################");
		System.out.println("**********Create Manual School**********");
		System.out.println("########################################");
		
		ExtractableResponse<Response> createManualSchoolResponse=
								given()
										.log().all()
										.header("Authorization", String.format("Bearer %s",getAccesToken()))
										.contentType("application/json")
										//.param("clientId","NonSPSClient")
										.body(createManualSchoolPayload()).
								when()
										.post(ENDPOINT_CREATE_MANUALSCHOOL).
								then()
										.statusCode(201)
										.spec(createManualSchoolResponseValidator())
										 .extract();	
			
		System.out.println(createManualSchoolResponse.asString());
	}
	
		
	
	@Test
	public void createHomeSchoolTest() 
	{	
		System.out.println("######################################");
		System.out.println("**********Create Home School**********");
		System.out.println("######################################");
		//ndW9LqFqW04jJO0mGyEriFNVfVga
		//QX507NurAlNremIGU6SjoeHPQ9oa
						
		ExtractableResponse<Response> createHomeSchoolResponse=
				given()
						.log().all()
						.header("Authorization", String.format("Bearer %s",getAccesToken()))
						.contentType("application/json")
						//.param("clientId","NonSPSClient")
						.body(createHomeSchoolPayload()).
				when()
						.post(ENDPOINT_CREATE_HOMESCHOOL).
				then()
						.statusCode(201)
						.spec(createHomeSchoolResponseValidator())
						 .extract();	

		System.out.println(createHomeSchoolResponse.asString());
	}
	
	@Test
	public void createLogInAndMakePaymentTest() 
	{	
		System.out.println("######################################################################");
		System.out.println("***Login to Create Session Id,TSP Id, Make Payment and Get Response***");
		System.out.println("######################################################################");
		//ndW9LqFqW04jJO0mGyEriFNVfVga
					
		ExtractableResponse<Response> createLogInResponse=
				given()
						.contentType("application/json")
		                .body(createLogInPayload()).
				when()
						.post("http://fs-iam-spsapifacade-qa1.scholastic-labs.io/sps-api-facade/spsuser/login?clientId=SPSFacadeAPI").
			    then()
						.statusCode(200)
						.extract();	
		System.out.println(createLogInResponse.asString());
		sps_session=createLogInResponse.path("SPS_SESSION.value");
		sps_tsp=createLogInResponse.path("SPS_TSP.value");
		userSPSID=((String)createLogInResponse.path("SPS_UD.value")).split("\\|")[0];
		
		System.out.println(">>>>>>>>>>> SPS Session Id: "+sps_session);
		System.out.println(">>>>>>>>>>> SPS TSP Id: "+sps_tsp);
		System.out.println(">>>>>>>>>>> User SPSID: "+userSPSID);
		
		//System.out.println("!!!!!!!"+createLogInResponse.asString());
		 
		//Make Payment
		ExtractableResponse<Response> createMakePaymentResponse=
				given()
						.log().all()
						.with()
						.header("Authorization", String.format("Bearer %s",getAccesToken()))
						.cookies("SPS_SESSION",sps_session)
						.cookie("SPS_TSP",sps_tsp)
						.contentType("application/x-www-form-urlencoded")
						.body(createMakePaymentPayload()).
				when()
						.post(ENDPOINT_MAKE_PAYMENT).
				then()
						.statusCode(200)
						 .extract();	

		//System.out.println(createMakePaymentResponse.asString());
		
		System.out.println("---------------------Make Payment Response--------------------------");
		System.out.println(createMakePaymentResponse.asString());		
		//String response=createMakePaymentResponse.asString().replaceAll("[{}]", "");
		//String []a=response.split(":");
		//String result=(a[1].replaceAll("\"", " ")).trim().toLowerCase();
		//Assert.assertTrue(result.equalsIgnoreCase("success"));
		
	}
	
	private String getTransactionId()
	{ 
		transactionId="436301"+String.valueOf(System.currentTimeMillis())+"MZR";
		return transactionId;
	
	}
	
	
	
	
	private Map<String, Object> createManualSchoolPayload()
	
	{
		Map<String, Object> createManualSchoolInfo = new HashMap<String, Object>();
		createManualSchoolInfo.put("name",m_name );
		createManualSchoolInfo.put("address1",m_address1);
		createManualSchoolInfo.put("address2",m_address2);
		createManualSchoolInfo.put("address3", m_address3);
		createManualSchoolInfo.put("address4",m_address4);
		createManualSchoolInfo.put("address5",m_address5);
		createManualSchoolInfo.put("city",m_city);
		createManualSchoolInfo.put("state", m_state);
		createManualSchoolInfo.put("zipcode",m_zipcode);
		createManualSchoolInfo.put("country",m_country);
		createManualSchoolInfo.put("poBox",m_poBox);
		createManualSchoolInfo.put("county",m_county);
		createManualSchoolInfo.put("phone", m_phone);
		createManualSchoolInfo.put("reportingSchoolType",m_reportingSchoolType);
		return createManualSchoolInfo;
		
	}
	
		private Map<String, Object> createHomeSchoolPayload()
	
	{
		Map<String, Object> createHomeSchoolInfo = new HashMap<String, Object>();
		createHomeSchoolInfo.put("name",h_name );
		createHomeSchoolInfo.put("address1",h_address1);
		createHomeSchoolInfo.put("address2",h_address2);
		createHomeSchoolInfo.put("address3", h_address3);
		createHomeSchoolInfo.put("address4",h_address4);
		createHomeSchoolInfo.put("address5",h_address5);
		createHomeSchoolInfo.put("city",h_city);
		createHomeSchoolInfo.put("state", h_state);
		createHomeSchoolInfo.put("zipcode",h_zipcode);
		createHomeSchoolInfo.put("country",h_country);
		createHomeSchoolInfo.put("poBox",h_poBox);
		createHomeSchoolInfo.put("county",h_county);
		createHomeSchoolInfo.put("phone", h_phone);
		createHomeSchoolInfo.put("reportingSchoolType",h_reportingSchoolType);
		return createHomeSchoolInfo;
		
	}
		
		private Map<String, Object> createLogInPayload()

		{
			Map<String, Object> createLogInInfo = new HashMap<String, Object>();
			createLogInInfo.put("user",user );
			createLogInInfo.put("password",password);
			createLogInInfo.put("singleToken",singleToken);
			createLogInInfo.put("userForWS", userForWS);
			createLogInInfo.put("addCookie",addCookie);
			createLogInInfo.put("addExpiration",addExpiration);
			return createLogInInfo;
		
		}
	
		@SuppressWarnings("unchecked")
		private String createMakePaymentPayload()
		
		{
			JSONObject createMakePaymentInfo=new JSONObject();
			createMakePaymentInfo.put("cardType",cardType );
			createMakePaymentInfo.put("cardExpMonth",cardExpMonth);
			createMakePaymentInfo.put("cardExpYear",cardExpYear);
			createMakePaymentInfo.put("firstName", firstName);
			createMakePaymentInfo.put("lastName",lastName);
			createMakePaymentInfo.put("cardSecurityCode","");
			createMakePaymentInfo.put("phone",phone);
			createMakePaymentInfo.put("cardNumber", cardNumber);
			createMakePaymentInfo.put("addrOne",addrOne);
			createMakePaymentInfo.put("addrTwo",addrTwo);
			createMakePaymentInfo.put("zip",zip);
			createMakePaymentInfo.put("spsId",userSPSID);
			createMakePaymentInfo.put("state",state);
			createMakePaymentInfo.put("country", country);
			createMakePaymentInfo.put("email",email);
			createMakePaymentInfo.put("chargeAmount",chargeAmount);
			createMakePaymentInfo.put("city",city);
			createMakePaymentInfo.put("paymentToken",paymentToken);
			createMakePaymentInfo.put("transactionId",getTransactionId());
			createMakePaymentInfo.put("authCode",authCode);
			createMakePaymentInfo.put("storeWalletFlag","");
			createMakePaymentInfo.put("authTime",authTime);
			createMakePaymentInfo.put("bcoe",bcoe);
			createMakePaymentInfo.put("reconcileId",reconcileId);
			
			return "json="+createMakePaymentInfo;
			
		}		
		private ResponseSpecification createManualSchoolResponseValidator()
	
		{
			ResponseSpecBuilder rspec=new ResponseSpecBuilder()
			.expectBody("name",equalTo("Manual School Inc. 121"))
			.expectBody("address1",equalTo("557 Broadway"))
			.expectBody("address2",isEmptyString())
			.expectBody("address3",isEmptyString())
			.expectBody("address4",isEmptyString())
			.expectBody("address5",isEmptyString())
			.expectBody("city",equalTo("NYC"))
			.expectBody("state",equalTo("NY"))
			.expectBody("country",isEmptyString())
			.expectBody("poBox",isEmptyString())
			.expectBody("phone",equalTo("2123436111"))
			.expectBody("reportingSchoolType",equalTo("PS"));
			return rspec.build();
		}	
	
		private ResponseSpecification createHomeSchoolResponseValidator()
	
	{
		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("name",equalTo("Home School Inc. 1212"))
		.expectBody("address1",equalTo("578 Broadway"))
		.expectBody("address2",isEmptyString())
		.expectBody("address3",isEmptyString())
		.expectBody("address4",isEmptyString())
		.expectBody("address5",isEmptyString())
		.expectBody("city",equalTo("NYC"))
		.expectBody("state",equalTo("NY"))
		.expectBody("country",isEmptyString())
		.expectBody("poBox",isEmptyString())
		.expectBody("phone",equalTo("2123432266"))
		.expectBody("reportingSchoolType",equalTo("HOS"));
		return rspec.build();
	}
		
		/*private ResponseSpecification createMakePaymentResponseValidator()
		
		{
			ResponseSpecBuilder rspec=new ResponseSpecBuilder()
			.expectBody("result",containsString("success"));
			return rspec.build();
		}*/
		
		
	

	private static void addKeyValueValidations(ResponseSpecBuilder specBuilder,	String[][] keyValues) 
	{
		specBuilder.expectBody("size()", equalTo(keyValues.length));
		for (int i = 0; i < keyValues.length; i++) 
		{
			specBuilder.expectBody("get(" + i + ").key",
			equalTo(keyValues[i][0]));
			specBuilder.expectBody("get(" + i + ").name",
			equalTo(keyValues[i][1]));
		}
	}
	
	private ResponseSpecification createEnumResponseValidator(String[][] keyvalues )
	{
		ResponseSpecBuilder respec=new ResponseSpecBuilder()
			.expectStatusCode(200)
			.expectContentType(ContentType.JSON);
			addKeyValueValidations(respec,keyvalues);
			return respec.build();
	}
	
}
