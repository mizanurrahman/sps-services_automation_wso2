package com.scholastic.facade.Automation_WS02;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.isEmptyString;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.jayway.restassured.builder.ResponseSpecBuilder;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.ExtractableResponse;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.ResponseSpecification;
public class SPSLookupServicesTest extends BaseTest 
{
	private String accessToken;
	private String orgSPSID ="264321";
	private String orgUCN;
	
	//private String user ="27selfbtsuser37@scholastic.com";
	//private String user ="peterkent@juno1.com";
	private String user ="27selfbtsuser112@scholastic.com";
	private String password ="passed1";
	private boolean singleToken = true;
	private boolean userForWS = false;
	private boolean addCookie = true;
	private boolean addExpiration = true;
	
	private String sps_session;
	private String sps_tsp;
	
	private static final String ENDPOINT_ALLSTATE="/app/sps-lookup/1.0/states";	
	private static final String ENDPOINT_ALLSTATE_USTERRITORIES="/app/sps-lookup/1.0/states?includeUSTerritories=Yes&";
	private static final String ENDPOINT_ALLCITY="/app/sps-lookup/1.0/states/NY/cities";
	private static final String ENDPOINT_CITYSTATE_BYZIPCODE="/app/sps-lookup/1.0/zipcode/11372/cities?ccState=true&";
	private static final String ENDPOINT_SCHOOLBYCITYSATATE="/app/sps-lookup/1.0/states/NY/cities/Alcove/school?forceClient=&";
	private static final String ENDPOINT_SCHOOLBYZIPCODE="/app/sps-lookup/1.0/zipcode/11431/school?forceClient=&";
	private static final String ENDPOINT_ORGINFO="/app/sps-organizations/1.0/{spsId}";
	//private static final String ENDPOINT_REQRESET_PASSWORD="/app/sps-resetpwd/1.0/ResetReqPwd/processRemote?SPSWSXML=%3CSchWS%3E%3Cattribute%20name=%22clientID%22%20value=%22BKFLX%22/%3E%3Cattribute%20name=%22landingPage%22%20value=%22www.desire.lading.page.com%22/%3E%3Cattribute%20name=%22emailID%22%20value=%22fulloa-consultant@scholastic.com%22/%3E%3C/SchWS%3E";
	private static final String ENDPOINT_BPSUMMARY="/user/bpbpsweb/SummaryServiceServlet";
	private static final String ENDPOINT_BPHISTORY="/user/bpbpsweb/HistoryServiceServlet";
	private static final String ENDPOINT_PAYMENT_HISTORY="/user/bpbpsweb/PaymentHistoryServlet";
	private static final String ENDPOINT_SCHOOLDISTRICT_PARENTORGS="/app/orgs/1.0/{ucn}/parent-orgs";
	private static final String ENDPOINT_SCHOOLDISTRICT_TOPPARENTORGS="/app/orgs/1.0/{ucn}/top-parent-orgs";
	private static final String ENDPOINT_SCHOOLDISTRICT_SUBORGS="/app/orgs/1.0/{ucn}/sub-orgs";
	//private static final String ENDPOINT_ORG_TYPE="/app/sps-expose/populateSchoolType";
	
	
	
	
	@Test
	public void getAllStateTest()
	{	
		System.out.println("###################################");
		System.out.println("**********Find All States**********");
		System.out.println("###################################");
		
		ExtractableResponse<Response> getAllStateResponse=
								given()
										.log().all()
										.with()
										.header("Authorization", String.format("Bearer %s",getAccesToken()))
										.param("clientId","NonSPSClient").
								when()
										.get(ENDPOINT_ALLSTATE).
								then()
										.statusCode(200)
										.spec(getAllStateResponseValidator())
										 .extract();	
			
		System.out.println(getAllStateResponse.asString());
	}
	
	@Test
	public void getAllStateWithUSTerritoryTest()
	{	
		System.out.println("#########################################################################");
		System.out.println("******************** Get All States With Territories ********************");
		System.out.println("#########################################################################");
		ExtractableResponse<Response> getAllStateUSTerritoriesResponse=
								given()
										.param("clientId","NonSPSClient").
								when()
										.get(ENDPOINT_ALLSTATE_USTERRITORIES).
								then()
										.statusCode(200)
										.spec(getAllStateUSTerritoriesResponseValidator())
										.extract();	
		System.out.println(getAllStateUSTerritoriesResponse.asString());
	}
	
	@Test
	public void getAllCityTest() 
	{	
		System.out.println("############################################");
		System.out.println("**********Find All Cities By State**********");
		System.out.println("############################################");
		//ndW9LqFqW04jJO0mGyEriFNVfVga
						
		ExtractableResponse<Response> getAllCityResponse=
								given()
										.log().all()
										.with()
										.header("Authorization", String.format("Bearer %s",getAccesToken()))
										.param("clientId","NonSPSClient").
								when()
										.get(ENDPOINT_ALLCITY).
								then()
										.statusCode(200)
										.spec(getAllCityResponseValidator())
										.extract();	
		System.out.println(getAllCityResponse.asString());
	}
	
	@Test
	public void getCityStateByZipcodeTest()
	{
		System.out.println("#################################################");
		System.out.println("**********Find City & State By Zip Code**********");
		System.out.println("#################################################");
		
		ExtractableResponse<Response> getCityStateByZipCodeResponse=
					given()
							.log().all()
							.with()
							.header("Authorization", String.format("Bearer %s",getAccesToken()))
							.param("clientId","NonSPSClient").
					when()
							.get(ENDPOINT_CITYSTATE_BYZIPCODE).
					then()
					 		.statusCode(200)
					 		.spec(getCityStateByZipCodeResponseValidator())
					 		.extract();		
		System.out.println(getCityStateByZipCodeResponse.asString());
	}
	
	@Test
	public void getSchoolByCityStateTest() 
	{
		System.out.println("###################################################");
		System.out.println("**********Find Schools By Citie and State**********");
		System.out.println("###################################################");
		
		ExtractableResponse<Response> getSchoolByCityState=
					given()
							.log().all()
							.with()
							.header("Authorization", String.format("Bearer %s",getAccesToken()))
							.param("clientId","NonSPSClient").
					when()
							.get(ENDPOINT_SCHOOLBYCITYSATATE).
					then()
			 				.statusCode(200)
			 				.spec(getSchoolByCityStateResponseValidator())
			 				.extract();		
		System.out.println(getSchoolByCityState.asString());
	}
	
	@Test
	public void getSchoolByZipcodeTest()
	{
		System.out.println("############################################");
		System.out.println("**********Find Schools By Zip Code**********");
		System.out.println("############################################");
		
		ExtractableResponse<Response> getSchoolByZipcodeResponse=
					given()
							.log().all()
							.with()
							.header("Authorization", String.format("Bearer %s",getAccesToken()))
							.param("clientId","NonSPSClient").
					when()
							.get(ENDPOINT_SCHOOLBYZIPCODE).
					then()
					 		.statusCode(200)
					 		.spec(getSchoolByZipcodeResponseValidator())
					 		.extract();		
		System.out.println(getSchoolByZipcodeResponse.asString());
	}
	
	@Test
	public void getOrgInfo()
	{
		System.out.println("##########################################");
		System.out.println("**********Find Organization Info**********");
		System.out.println("##########################################");
		
		ExtractableResponse<Response> getOrgInfoResponse=
					given()
							.log().all()
							.with()
							.header("Authorization", String.format("Bearer %s",getAccesToken()))
							.pathParam("spsId",orgSPSID)
							.param("clientId","NonSPSClient").
					when()
							.get(ENDPOINT_ORGINFO).
					then()
					 		.statusCode(200)
					 		.spec(getOrgInfoResponseValidator())
					 		.extract();		
		System.out.println(getOrgInfoResponse.asString());
	}
	
	@Test
	public void createLogInAndGetBonusPointSummaryTest() 
	{	
		System.out.println("#######################################################################");
		System.out.println("***Login to Create Session Id,TSP Id and Get the Bonus Point Summary***");
		System.out.println("#######################################################################");
		//ndW9LqFqW04jJO0mGyEriFNVfVga
						
		ExtractableResponse<Response> createLogInResponse=
				given()
						.log().all()
						.contentType("application/json")
						.body(createLogInPayload()).
				when()
						.post("http://fs-iam-spsapifacade-qa1.scholastic-labs.io/sps-api-facade/spsuser/login?clientId=SPSFacadeAPI").
				then()
						.statusCode(200)
						.extract();	
		sps_session=createLogInResponse.path("SPS_SESSION.value");
		sps_tsp=createLogInResponse.path("SPS_TSP.value");
		
		System.out.println(">>>>>>>>>>> SPS Session Id: "+sps_session);
		System.out.println(">>>>>>>>>>> SPS TSP Id: "+sps_tsp);
		 
		//Get
		ExtractableResponse<Response> bonusPoinSummaryresponse=
				given()
						.log().all()
						.with()
						.header("Authorization", String.format("Bearer %s",getAccesToken()))
						.cookies("SPS_SESSION",sps_session)
						.cookie("SPS_TSP",sps_tsp).
				when()
						.get(ENDPOINT_BPSUMMARY).
				then()
						.statusCode(200)
						.extract();
						//.spec(createHomeSchoolResponseValidator())
		
		
		System.out.println("---------------------Bonus Point Summary--------------------------");
				
		System.out.println(bonusPoinSummaryresponse.asString());
		
	}
	
	@Test
	public void createLogInAndGetBonusPointHistoryTest() 
	{	
		System.out.println("#######################################################################");
		System.out.println("***Login to Create Session Id,TSP Id and Get the Bonus Point History***");
		System.out.println("#######################################################################");
		//ndW9LqFqW04jJO0mGyEriFNVfVga
						
		ExtractableResponse<Response> createLogInResponse=
				given()
						.log().all()
						.contentType("application/json")
						.body(createLogInPayload()).
				when()
						.post("http://fs-iam-spsapifacade-qa1.scholastic-labs.io/sps-api-facade/spsuser/login?clientId=SPSFacadeAPI").
				then()
						.statusCode(200)
						.extract();	
		sps_session=createLogInResponse.path("SPS_SESSION.value");
		sps_tsp=createLogInResponse.path("SPS_TSP.value");
		
		System.out.println(">>>>>>>>>>> SPS Session Id: "+sps_session);
		System.out.println(">>>>>>>>>>> SPS TSP Id: "+sps_tsp);
		 
		//Get
		ExtractableResponse<Response> bonusPointHistoryresponse=
				given()
						.log().all()
						.with()
						.header("Authorization", String.format("Bearer %s",getAccesToken()))
						.cookies("SPS_SESSION",sps_session)
						.cookie("SPS_TSP",sps_tsp).
				when()
						.get(ENDPOINT_BPHISTORY).
				then()
						.statusCode(200)
						.extract();
						//.spec(createHomeSchoolResponseValidator())
		
		
		System.out.println("---------------------Bonus Point History--------------------------");
				
		System.out.println(bonusPointHistoryresponse.asString());
		
	}
	
	@Test
	public void createLogInAndGetPaymentHistoryTest() 
	{	
		System.out.println("###################################################################");
		System.out.println("***Login to Create Session Id,TSP Id and Get the Payment History***");
		System.out.println("###################################################################");
		//ndW9LqFqW04jJO0mGyEriFNVfVga
						
		ExtractableResponse<Response> createLogInResponse=
				given()
						.log().all()
						.contentType("application/json")
						.body(createLogInPayload()).
				when()
						.post("http://fs-iam-spsapifacade-qa1.scholastic-labs.io/sps-api-facade/spsuser/login?clientId=SPSFacadeAPI").
				then()
						.statusCode(200)
						.extract();	
		sps_session=createLogInResponse.path("SPS_SESSION.value");
		sps_tsp=createLogInResponse.path("SPS_TSP.value");
		
		System.out.println(">>>>>>>>>>> SPS Session Id: "+sps_session);
		System.out.println(">>>>>>>>>>> SPS TSP Id: "+sps_tsp);
		 
		//Get
		ExtractableResponse<Response> paymentHistoryResponse=
				given()
						.log().all()
						.with()
						.header("Authorization", String.format("Bearer %s",getAccesToken()))
						.cookies("SPS_SESSION",sps_session)
						.cookie("SPS_TSP",sps_tsp).
				when()
						.get(ENDPOINT_PAYMENT_HISTORY).
				then()
						.statusCode(200)
						.extract();
						//.spec(paymentHistoryResponseValidator())
		
		
		System.out.println("---------------------Payment History--------------------------");
				
		System.out.println(paymentHistoryResponse.asString());
		
	}
	
	@Test
	public void getSchoolDistrictParentOrgs()
	{
		System.out.println("#######################################################");
		System.out.println("**********Find School District - /parent-orgs**********");
		System.out.println("#######################################################");
		
		orgUCN ="600072647";
		ExtractableResponse<Response> getSchoolDistrictParentOrgsResponse=
					given()
							.log().all()
							.with()
							.header("Authorization", String.format("Bearer %s",getAccesToken()))
							.pathParam("ucn",orgUCN).
							//.param("clientId","NonSPSClient").
					when()
							.get(ENDPOINT_SCHOOLDISTRICT_PARENTORGS).
					then()
					 		.statusCode(200)
					 		.spec(getSchoolDistrictParentOrgsResponseValidator())
					 		.extract();		
		System.out.println(getSchoolDistrictParentOrgsResponse.asString());
	}
	
	@Test
	public void getSchoolDistrictTopParentOrg()
	{
		System.out.println("######################################################");
		System.out.println("**********School District - /top-parent-orgs**********");
		System.out.println("######################################################");
		
		orgUCN ="600072647";
		ExtractableResponse<Response> getSchoolDistrictTopParentOrgResponse=
					given()
							.log().all()
							.with()
							.header("Authorization", String.format("Bearer %s",getAccesToken()))
							.pathParam("ucn",orgUCN).
							//.param("clientId","NonSPSClient").
					when()
							.get(ENDPOINT_SCHOOLDISTRICT_TOPPARENTORGS).
					then()
					 		.statusCode(200)
					 		.spec(getSchoolDistrictTopParentOrgResponseValidator())
					 		.extract();		
		System.out.println(getSchoolDistrictTopParentOrgResponse.asString());
	}
	
	@Test
	public void getSchoolDistrictSubOrgs()
	{
		System.out.println("###############################################");
		System.out.println("**********School District - /sub-orgs**********");
		System.out.println("###############################################");
		
		orgUCN ="600005892";
		ExtractableResponse<Response> getSchoolDistrictSubOrgsResponse=
					given()
							.log().all()
							.with()
							.header("Authorization", String.format("Bearer %s",getAccesToken()))
							.pathParam("ucn",orgUCN).
							//.param("clientId","NonSPSClient").
					when()
							.get(ENDPOINT_SCHOOLDISTRICT_SUBORGS).
					then()
					 		.statusCode(200)
					 		.spec(getSchoolDistrictSubOrgsResponseValidator())
					 		.extract();		
		System.out.println(getSchoolDistrictSubOrgsResponse.asString());
	}
	
	/*@Test
	public void getOrgType()
	{
		System.out.println("#################################");
		System.out.println("**********Get Org Types**********");
		System.out.println("#################################");
		
		ExtractableResponse<Response> getSchoolDistrictSubOrgsResponse=
					given()
							.with()
							.header("Authorization", String.format("Bearer %s",getAccesToken()))
							.pathParam("ucn",orgUCN)
							.param("clientId","RCO").
					when()
							.get(ENDPOINT_ORG_TYPE).
					then()
					 		.statusCode(200)
					 		//.spec(getSchoolDistrictSubOrgsResponseValidator())
					 		.extract();		
		System.out.println(getSchoolDistrictSubOrgsResponse.asString());
	}*/
	
	/*@Test
	public void requestResetPasswordTest() 
	{
		System.out.println("###################################################");
		System.out.println("**********Request Reset Password**********");
		System.out.println("###################################################");
		
		ExtractableResponse<Response> requestResetPassword=
					given() .
							//.with()
							//.header("Authorization", String.format("Bearer %s",getAccesToken())).
							//.param("clientId","NonSPSClient").
					when()
							.get(ENDPOINT_REQRESET_PASSWORD).
					then()
			 				.statusCode(200)
			 				//.spec(requestResetPasswordResponseValidator())
			 				.extract();		
		System.out.println(requestResetPassword.asString());
	}*/
	
	
	/*private String getAccesToken()
	{
		accessToken=System.getProperty("WSO2AccessToken");
		System.out.println("accessToken ............"+accessToken);
		return accessToken;
	}*/
		
	private ResponseSpecification getAllStateResponseValidator()
	{
		final String[][] keyValues = 
			{ 
			{ "AK", "AK - Alaska" }, { "AL", "AL - Alabama" }, { "AR", "AR - Arkansas" },
			{ "AS", "AS - American Samoa" }, { "AZ", "AZ - Arizona" },{"CA","CA - California" }, { "CO", "CO - Colorado" },
			{ "CT", "CT - Connecticut" }, { "DC", "DC - District of Columbia" }, { "DE", "DE - Delaware" }, 
			{ "FL", "FL - Florida" }, { "FM", "FM - Micronesia" }, { "GA", "GA - Georgia" }, { "GU", "GU - Guam" },
			{ "HI", "HI - Hawaii" }, { "IA", "IA - Iowa" }, { "ID", "ID - Idaho" }, { "IL", "IL - Illinois" }, 
			{ "IN", "IN - Indiana" }, { "KS", "KS - Kansas" }, { "KY", "KY - Kentucky" }, { "LA", "LA - Louisiana"}, 
			{ "MA", "MA - Massachusetts" }, { "MD", "MD - Maryland" }, { "ME", "ME - Maine" }, { "MH", "MH - Marshall Isl"},
			{ "MI", "MI - Michigan" }, { "MN", "MN - Minnesota" }, { "MO", "MO - Missouri" }, { "MP", "MP - Northern Mariana Isls" },
			{ "MS", "MS - Mississippi" }, { "MT", "MT - Montana" }, { "NC", "NC - North Carolina" }, { "ND", "ND - North Dakota" },
			{ "NE", "NE - Nebraska" }, { "NH", "NH - New Hampshire" }, { "NJ", "NJ - New Jersey" }, { "NM", "NM - New Mexico" },
			{ "NV", "NV - Nevada" }, { "NY", "NY - New York" }, { "OH", "OH - Ohio" }, { "OK", "OK - Oklahoma" }, { "OR", "OR - Oregon" },
			{ "PA", "PA - Pennsylvania" }, { "PR", "PR - Puerto Rico" }, { "PW", "PW - Palau" }, { "RI", "RI - Rhode Island" }, 
			{ "SC", "SC - South Carolina" }, { "SD", "SD - South Dakota" }, { "TN", "TN - Tennessee" }, { "TX", "TX - Texas" }, 
			{ "UM", "UM - US Minor Outlying Isls" }, { "UT", "UT - Utah" }, { "VA", "VA - Virginia" }, { "VI", "VI - Virgin Islands" },
			{ "VT", "VT - Vermont" }, { "WA", "WA - Washington" }, { "WI", "WI - Wisconsin" }, { "WV", "WV - West Virginia" }, { "WY", "WY - Wyoming" }
			};

			return createEnumResponseValidator(keyValues);
	}
	
	private ResponseSpecification getAllStateUSTerritoriesResponseValidator()
	{
		final String[][] keyValues = 
			{ 
			{"AA", "AA - Other Americas"}, {"AE", "AE - Military Facilities"}, { "AK", "AK - Alaska" }, { "AL", "AL - Alabama" }, {"AP", "AP - Intl Mail - Pacific"}, { "AR", "AR - Arkansas" },
			{ "AS", "AS - American Samoa" }, { "AZ", "AZ - Arizona" },{"CA","CA - California" }, { "CO", "CO - Colorado" },
			{ "CT", "CT - Connecticut" }, { "DC", "DC - District of Columbia" }, { "DE", "DE - Delaware" }, 
			{ "FL", "FL - Florida" }, { "FM", "FM - Micronesia" }, { "GA", "GA - Georgia" }, { "GU", "GU - Guam" },
			{ "HI", "HI - Hawaii" }, { "IA", "IA - Iowa" }, { "ID", "ID - Idaho" }, { "IL", "IL - Illinois" }, 
			{ "IN", "IN - Indiana" }, { "KS", "KS - Kansas" }, { "KY", "KY - Kentucky" }, { "LA", "LA - Louisiana"}, 
			{ "MA", "MA - Massachusetts" }, { "MD", "MD - Maryland" }, { "ME", "ME - Maine" }, { "MH", "MH - Marshall Isl"},
			{ "MI", "MI - Michigan" }, { "MN", "MN - Minnesota" }, { "MO", "MO - Missouri" }, { "MP", "MP - Northern Mariana Isls" },
			{ "MS", "MS - Mississippi" }, { "MT", "MT - Montana" }, { "NC", "NC - North Carolina" }, { "ND", "ND - North Dakota" },
			{ "NE", "NE - Nebraska" }, { "NH", "NH - New Hampshire" }, { "NJ", "NJ - New Jersey" }, { "NM", "NM - New Mexico" },
			{ "NV", "NV - Nevada" }, { "NY", "NY - New York" }, { "OH", "OH - Ohio" }, { "OK", "OK - Oklahoma" }, { "OR", "OR - Oregon" },
			{ "PA", "PA - Pennsylvania" }, { "PR", "PR - Puerto Rico" }, { "PW", "PW - Palau" }, { "RI", "RI - Rhode Island" }, 
			{ "SC", "SC - South Carolina" }, { "SD", "SD - South Dakota" }, { "TN", "TN - Tennessee" }, { "TX", "TX - Texas" }, 
			{ "UM", "UM - US Minor Outlying Isls" }, { "UT", "UT - Utah" }, { "VA", "VA - Virginia" }, { "VI", "VI - Virgin Islands" },
			{ "VT", "VT - Vermont" }, { "WA", "WA - Washington" }, { "WI", "WI - Wisconsin" }, { "WV", "WV - West Virginia" }, { "WY", "WY - Wyoming" }
			};

			return createEnumResponseValidator(keyValues);
	}
	
	private ResponseSpecification getAllCityResponseValidator()
	{
		final String[][] keyValues = 
			{ 
				{ "Accord", "Accord" }, { "Acra", "Acra" }, { "Adams", "Adams" }, { "Adams Basin", "Adams Basin" }, { "Adams Center", "Adams Center" }, 
			    { "Addisleigh Park", "Addisleigh Park" }, { "Addison", "Addison" }, { "Adirondack", "Adirondack" }, { "Afton", "Afton" }, { "Airmont", "Airmont" },
			    { "Akron", "Akron" }, { "Akwesasne", "Akwesasne" }, { "Alabama", "Alabama" }, { "Albany", "Albany" }, { "Albertson", "Albertson" }, 
			    { "Albion", "Albion" }, { "Alcove", "Alcove" }, { "Alden", "Alden" }, { "Alden Manor", "Alden Manor" }, { "Alder Creek", "Alder Creek" },
			    { "Alexander", "Alexander" }, { "Alexandria Bay", "Alexandria Bay" }, { "Alfred", "Alfred" }, { "Alfred Station", "Alfred Station" },
			    { "Allegany", "Allegany" }, { "Allentown", "Allentown" }, { "Alma", "Alma" }, { "Almond", "Almond" }, { "Alpine", "Alpine" }, { "Alplaus", "Alplaus" },
			    { "Altamont", "Altamont" }, { "Altmar", "Altmar" }, { "Alton", "Alton" }, { "Altona", "Altona" }, { "Amagansett", "Amagansett" }, { "Amawalk", "Amawalk" },
			    { "Amenia", "Amenia" }, { "Ames", "Ames" }, { "Amherst", "Amherst" }, { "Amity Harbor", "Amity Harbor" }, { "Amityville", "Amityville" }, 
			    { "Amsterdam", "Amsterdam" }, { "Ancram", "Ancram" }, { "Ancramdale", "Ancramdale" }, { "Andes", "Andes" }, { "Andover", "Andover" },
			    { "Angelica", "Angelica" }, { "Angola", "Angola" }, { "Annandale", "Annandale" }, { "Annandale On Hudson", "Annandale On Hudson" }, { "Antwerp", "Antwerp" },
			    { "Apalachin", "Apalachin" }, { "Appleton", "Appleton" }, { "Apulia Station", "Apulia Station" }, { "Aquebogue", "Aquebogue" }, { "Arcade", "Arcade" },
			    { "Arden", "Arden" }, { "Ardsley", "Ardsley" }, { "Ardsley On Hudson", "Ardsley On Hudson" }, { "Argyle", "Argyle" }, { "Arkport", "Arkport" },
			    { "Arkville", "Arkville" }, { "Arlington", "Arlington" }, { "Armonk", "Armonk" }, { "Arverne", "Arverne" }, { "Ashland", "Ashland" }, { "Ashville", "Ashville" },
			    { "Astoria", "Astoria" }, { "Athens", "Athens" }, { "Athol", "Athol" }, { "Athol Springs", "Athol Springs" }, { "Atlanta", "Atlanta" }, 
			    { "Atlantic Beach", "Atlantic Beach" }, { "Attica", "Attica" }, { "Au Sable Chasm", "Au Sable Chasm" }, { "Au Sable Forks", "Au Sable Forks" },
			    { "Auburn", "Auburn" }, { "Auburndale", "Auburndale" }, { "Auriesville", "Auriesville" }, { "Aurora", "Aurora" }, { "Ausable Chasm", "Ausable Chasm" },
			    { "Austerlitz", "Austerlitz" }, { "Ava", "Ava" }, { "Averill Park", "Averill Park" }, { "Avoca", "Avoca" }, { "Avon", "Avon" }, { "Babylon", "Babylon" },
			    { "Bainbridge", "Bainbridge" }, { "Baiting Hollow", "Baiting Hollow" }, { "Bakers Mills", "Bakers Mills" }, { "Baldwin", "Baldwin" },
			    { "Baldwin Place", "Baldwin Place" }, { "Baldwinsville", "Baldwinsville" }, { "Ballston Lake", "Ballston Lake" }, { "Ballston Spa", "Ballston Spa" },
			    { "Balmat", "Balmat" }, { "Bangall", "Bangall" }, { "Bangor", "Bangor" }, { "Bardonia", "Bardonia" }, { "Barker", "Barker" },
			    { "Barnes Corners", "Barnes Corners" }, { "Barneveld", "Barneveld" }, { "Barrytown", "Barrytown" }, { "Barryville", "Barryville" },
			    { "Barton", "Barton" }, { "Basom", "Basom" }, { "Batavia", "Batavia" }, { "Bath", "Bath" }, { "Bay Shore", "Bay Shore" }, { "Bayberry", "Bayberry" },
			    { "Bayport", "Bayport" }, { "Bayside", "Bayside" }, { "Bayside Hills", "Bayside Hills" }, { "Bayville", "Bayville" }, { "Beacon", "Beacon" }, 
			    { "Bear Mountain", "Bear Mountain" }, { "Bearsville", "Bearsville" }, { "Beaver Dams", "Beaver Dams" }, { "Beaver Falls", "Beaver Falls" }, 
			    { "Beaver River", "Beaver River" }, { "Bedford", "Bedford" }, { "Bedford Corners", "Bedford Corners" }, { "Bedford Hills", "Bedford Hills" }, 
			    { "Beechhurst", "Beechhurst" }, { "Belfast", "Belfast" }, { "Belle Harbor", "Belle Harbor" }, { "Bellerose", "Bellerose" }, 
			    { "Bellerose Manor", "Bellerose Manor" }, { "Bellerose Village", "Bellerose Village" }, { "Belleville", "Belleville" }, { "Bellmore", "Bellmore" },
			    { "Bellona", "Bellona" }, { "Bellport", "Bellport" }, { "Bellvale", "Bellvale" }, { "Belmont", "Belmont" }, { "Bemus Point", "Bemus Point" },
			    { "Bergen", "Bergen" }, { "Berkshire", "Berkshire" }, { "Berlin", "Berlin" }, { "Berne", "Berne" }, { "Bernhards Bay", "Bernhards Bay" }, 
			    { "Bethel", "Bethel" }, { "Bethpage", "Bethpage" }, { "Bible School Park", "Bible School Park" }, { "Big Flats", "Big Flats" },
			    { "Big Indian", "Big Indian" }, { "Billings", "Billings" }, { "Binghamton", "Binghamton" }, { "Black Creek", "Black Creek" },
			    { "Black River", "Black River" }, { "Blasdell", "Blasdell" }, { "Blauvelt", "Blauvelt" }, { "Bliss", "Bliss" }, { "Blodgett Mills", "Blodgett Mills" },
			    { "Bloomfield", "Bloomfield" }, { "Blooming Grove", "Blooming Grove" }, { "Bloomingburg", "Bloomingburg" }, { "Bloomingdale", "Bloomingdale" },
			    { "Bloomington", "Bloomington" }, { "Bloomville", "Bloomville" }, { "Blossvale", "Blossvale" }, { "Blue Mountain Lake", "Blue Mountain Lake" },
			    { "Blue Point", "Blue Point" }, { "Bluff Point", "Bluff Point" }, { "Bohemia", "Bohemia" }, { "Boiceville", "Boiceville" }, { "Bolivar", "Bolivar" },
			    { "Bolton Landing", "Bolton Landing" }, { "Bombay", "Bombay" }, { "Boonville", "Boonville" }, { "Boston", "Boston" }, { "Bouckville", "Bouckville" },
			    { "Bovina Center", "Bovina Center" }, { "Bowmansville", "Bowmansville" }, { "Bradford", "Bradford" }, { "Brainard", "Brainard" }, 
			    { "Brainardsville", "Brainardsville" }, { "Branchport", "Branchport" }, { "Brant", "Brant" }, { "Brant Lake", "Brant Lake" },
			    { "Brantingham", "Brantingham" }, { "Brasher Falls", "Brasher Falls" }, { "Breesport", "Breesport" }, { "Breezy Point", "Breezy Point" },
			    { "Brentwood", "Brentwood" }, { "Brewerton", "Brewerton" }, { "Brewster", "Brewster" }, { "Briarcliff", "Briarcliff" }, 
			    { "Briarcliff Manor", "Briarcliff Manor" }, { "Briarwood", "Briarwood" }, { "Bridgehampton", "Bridgehampton" }, { "Bridgeport", "Bridgeport" },
			    { "Bridgewater", "Bridgewater" }, { "Brier Hill", "Brier Hill" }, { "Brighton", "Brighton" }, { "Brightwaters", "Brightwaters" }, { "Brisben", "Brisben" },
			    { "Broad Channel", "Broad Channel" }, { "Broadalbin", "Broadalbin" }, { "Brockport", "Brockport" }, { "Brocton", "Brocton" }, { "Bronx", "Bronx" },
			    { "Bronxville", "Bronxville" }, { "Brookfield", "Brookfield" }, { "Brookhaven", "Brookhaven" }, { "Brooklyn", "Brooklyn" },
			    { "Brooklyn Heights", "Brooklyn Heights" }, { "Brooktondale", "Brooktondale" }, { "Brookview", "Brookview" }, { "Brownville", "Brownville" }, 
			    { "Brushton", "Brushton" }, { "Buchanan", "Buchanan" }, { "Buffalo", "Buffalo" }, { "Bullville", "Bullville" }, { "Burdett", "Burdett" },
			    { "Burke", "Burke" }, { "Burlingham", "Burlingham" }, { "Burlington Flats", "Burlington Flats" }, { "Burnt Hills", "Burnt Hills" }, { "Burt", "Burt" },
			    { "Buskirk", "Buskirk" }, { "Byron", "Byron" }, { "Cadosia", "Cadosia" }, { "Cadyville", "Cadyville" }, { "Cairo", "Cairo" }, { "Calcium", "Calcium" },
			    { "Caledonia", "Caledonia" }, { "Callicoon", "Callicoon" }, { "Callicoon Center", "Callicoon Center" }, { "Calverton", "Calverton" }, 
			    { "Cambria Heights", "Cambria Heights" }, { "Cambridge", "Cambridge" }, { "Camden", "Camden" }, { "Cameron", "Cameron" }, { "Cameron Mills", "Cameron Mills" },
			    { "Camillus", "Camillus" }, { "Campbell", "Campbell" }, { "Campbell Hall", "Campbell Hall" }, { "Canaan", "Canaan" }, { "Canajoharie", "Canajoharie" },
			    { "Canandaigua", "Canandaigua" }, { "Canaseraga", "Canaseraga" }, { "Canastota", "Canastota" }, { "Candor", "Candor" }, { "Caneadea", "Caneadea" },
			    { "Canisteo", "Canisteo" }, { "Canton", "Canton" }, { "Cape Vincent", "Cape Vincent" }, { "Captree Island", "Captree Island" }, 
			    { "Carle Place", "Carle Place" }, { "Carlisle", "Carlisle" }, { "Carmel", "Carmel" }, { "Caroga Lake", "Caroga Lake" }, { "Carthage", "Carthage" },
			    { "Cassadaga", "Cassadaga" }, { "Cassville", "Cassville" }, { "Castile", "Castile" }, { "Castle Creek", "Castle Creek" }, { "Castle Point", "Castle Point" },
			    { "Castleton", "Castleton" }, { "Castleton On Hudson", "Castleton On Hudson" }, { "Castorland", "Castorland" }, { "Cato", "Cato" }, { "Catskill", "Catskill" },
			    { "Cattaraugus", "Cattaraugus" }, { "Cayuga", "Cayuga" }, { "Cayuta", "Cayuta" }, { "Cazenovia", "Cazenovia" }, { "Cedarhurst", "Cedarhurst" },
			    { "Celoron", "Celoron" }, { "Cementon", "Cementon" }, { "Center Moriches", "Center Moriches" }, { "Centereach", "Centereach" }, 
			    { "Centerport", "Centerport" }, { "Centerville", "Centerville" }, { "Central Bridge", "Central Bridge" }, { "Central Islip", "Central Islip" },
			    { "Central Square", "Central Square" }, { "Central Valley", "Central Valley" }, { "Ceres", "Ceres" }, { "Chadwicks", "Chadwicks" },
			    { "Chaffee", "Chaffee" }, { "Champlain", "Champlain" }, { "Chappaqua", "Chappaqua" }, { "Charlotteville", "Charlotteville" }, 
			    { "Charlton", "Charlton" }, { "Chase Mills", "Chase Mills" }, { "Chateaugay", "Chateaugay" }, { "Chatham", "Chatham" }, { "Chaumont", "Chaumont" },
			    { "Chautauqua", "Chautauqua" }, { "Chazy", "Chazy" }, { "Cheektowaga", "Cheektowaga" }, { "Chelsea", "Chelsea" }, { "Chemung", "Chemung" },
			    { "Chenango Bridge", "Chenango Bridge" }, { "Chenango Forks", "Chenango Forks" }, { "Cherry Creek", "Cherry Creek" }, { "Cherry Grove", "Cherry Grove" },
			    { "Cherry Plain", "Cherry Plain" }, { "Cherry Valley", "Cherry Valley" }, { "Chester", "Chester" }, { "Chestertown", "Chestertown" },
			    { "Chestnut Ridge", "Chestnut Ridge" }, { "Chichester", "Chichester" }, { "Childwold", "Childwold" }, { "Chippewa Bay", "Chippewa Bay" },
			    { "Chittenango", "Chittenango" }, { "Churchville", "Churchville" }, { "Churubusco", "Churubusco" }, { "Cicero", "Cicero" }, 
			    { "Cincinnatus", "Cincinnatus" }, { "Circleville", "Circleville" }, { "Clarence", "Clarence" }, { "Clarence Center", "Clarence Center" }, 
			    { "Clarendon", "Clarendon" }, { "Clark Mills", "Clark Mills" }, { "Clarkson", "Clarkson" }, { "Clarksville", "Clarksville" },
			    { "Claryville", "Claryville" }, { "Claverack", "Claverack" }, { "Clay", "Clay" }, { "Clayton", "Clayton" }, { "Clayville", "Clayville" }, 
			    { "Clemons", "Clemons" }, { "Cleveland", "Cleveland" }, { "Cleverdale", "Cleverdale" }, { "Clifton", "Clifton" }, { "Clifton Park", "Clifton Park" },
			    { "Clifton Springs", "Clifton Springs" }, { "Climax", "Climax" }, { "Clinton", "Clinton" }, { "Clinton Corners", "Clinton Corners" }, 
			    { "Clintondale", "Clintondale" }, { "Clintonville", "Clintonville" }, { "Clockville", "Clockville" }, { "Clyde", "Clyde" },
			    { "Clymer", "Clymer" }, { "Cobleskill", "Cobleskill" }, { "Cochecton", "Cochecton" }, { "Cochecton Center", "Cochecton Center" },
			    { "Coeymans", "Coeymans" }, { "Coeymans Hollow", "Coeymans Hollow" }, { "Cohocton", "Cohocton" }, { "Cohoes", "Cohoes" }, { "Cold Brook", "Cold Brook" },
			    { "Cold Spring", "Cold Spring" }, { "Cold Spring Harbor", "Cold Spring Harbor" }, { "Colden", "Colden" }, { "College Point", "College Point" },
			    { "Colliersville", "Colliersville" }, { "Collins", "Collins" }, { "Collins Center", "Collins Center" }, { "Colonie", "Colonie" }, 
			    { "Colton", "Colton" }, { "Columbiaville", "Columbiaville" }, { "Commack", "Commack" }, { "Comstock", "Comstock" }, { "Concord", "Concord" },
			    { "Conesus", "Conesus" }, { "Conewango Valley", "Conewango Valley" }, { "Congers", "Congers" }, { "Conklin", "Conklin" }, { "Connelly", "Connelly" },
			    { "Constable", "Constable" }, { "Constableville", "Constableville" }, { "Constantia", "Constantia" }, { "Coopers Plains", "Coopers Plains" },
			    { "Cooperstown", "Cooperstown" }, { "Copake", "Copake" }, { "Copake Falls", "Copake Falls" }, { "Copenhagen", "Copenhagen" }, { "Copiague", "Copiague" },
			    { "Coram", "Coram" }, { "Corbettsville", "Corbettsville" }, { "Corfu", "Corfu" }, { "Corinth", "Corinth" }, { "Corning", "Corning" }, { "Cornwall", "Cornwall" },
			    { "Cornwall On Hudson", "Cornwall On Hudson" }, { "Cornwallville", "Cornwallville" }, { "Corona", "Corona" }, { "Cortland", "Cortland" }, 
			    { "Cortlandt Manor", "Cortlandt Manor" }, { "Cossayuna", "Cossayuna" }, { "Cottekill", "Cottekill" }, { "Cowlesville", "Cowlesville" },
			    { "Coxsackie", "Coxsackie" }, { "Cragsmoor", "Cragsmoor" }, { "Cranberry Lake", "Cranberry Lake" }, { "Craryville", "Craryville" },
			    { "Crittenden", "Crittenden" }, { "Croghan", "Croghan" }, { "Crompond", "Crompond" }, { "Cropseyville", "Cropseyville" }, { "Cross River", "Cross River" },
			    { "Croton Falls", "Croton Falls" }, { "Croton On Hudson", "Croton On Hudson" }, { "Crown Point", "Crown Point" }, { "Crugers", "Crugers" },
			    { "Cuba", "Cuba" }, { "Cuddebackville", "Cuddebackville" }, { "Cutchogue", "Cutchogue" }, { "Cuyler", "Cuyler" }, { "Dale", "Dale" }, 
			    { "Dalton", "Dalton" }, { "Dannemora", "Dannemora" }, { "Dansville", "Dansville" }, { "Darien Center", "Darien Center" }, 
			    { "Davenport", "Davenport" }, { "Davenport Center", "Davenport Center" }, { "Davis Park", "Davis Park" }, { "Dayton", "Dayton" }, 
			    { "De Peyster", "De Peyster" }, { "De Ruyter", "De Ruyter" }, { "Deansboro", "Deansboro" }, { "Deer Park", "Deer Park" }, 
			    { "Deer River", "Deer River" }, { "Deerfield", "Deerfield" }, { "Deferiet", "Deferiet" }, { "Degrasse", "Degrasse" },
			    { "Dekalb Jct", "Dekalb Jct" }, { "Dekalb Junction", "Dekalb Junction" }, { "Delancey", "Delancey" }, { "Delanson", "Delanson" },
			    { "Delevan", "Delevan" }, { "Delhi", "Delhi" }, { "Delmar", "Delmar" }, { "Delphi Falls", "Delphi Falls" }, { "Denmark", "Denmark" },
			    { "Denver", "Denver" }, { "Depauville", "Depauville" }, { "Depew", "Depew" }, { "Deposit", "Deposit" }, { "Derby", "Derby" }, 
			    { "Dewittville", "Dewittville" }, { "Dexter", "Dexter" }, { "Diamond Point", "Diamond Point" }, { "Dickinson Center", "Dickinson Center" },
			    { "Dix Hills", "Dix Hills" }, { "Dobbs Ferry", "Dobbs Ferry" }, { "Dolgeville", "Dolgeville" }, { "Dormansville", "Dormansville" }, 
			    { "Douglaston", "Douglaston" }, { "Dover Plains", "Dover Plains" }, { "Downsville", "Downsville" }, { "Dresden", "Dresden" }, 
			    { "Dryden", "Dryden" }, { "Duanesburg", "Duanesburg" }, { "Dundee", "Dundee" }, { "Dunkirk", "Dunkirk" }, { "Durham", "Durham" },
			    { "Durhamville", "Durhamville" }, { "Eagle Bay", "Eagle Bay" }, { "Eagle Bridge", "Eagle Bridge" }, { "Eagle Harbor", "Eagle Harbor" },
			    { "Earlton", "Earlton" }, { "Earlville", "Earlville" }, { "East Amherst", "East Amherst" }, { "East Atlantic Beach", "East Atlantic Beach" },
			    { "East Aurora", "East Aurora" }, { "East Berne", "East Berne" }, { "East Bethany", "East Bethany" }, { "East Bloomfield", "East Bloomfield" },
			    { "East Branch", "East Branch" }, { "East Chatham", "East Chatham" }, { "East Concord", "East Concord" }, { "East Durham", "East Durham" },
			    { "East Elmhurst", "East Elmhurst" }, { "East Fishkill", "East Fishkill" }, { "East Freetown", "East Freetown" }, { "East Greenbush", "East Greenbush" },
			    { "East Greenwich", "East Greenwich" }, { "East Hampton", "East Hampton" }, { "East Homer", "East Homer" }, { "East Islip", "East Islip" },
			    { "East Jewett", "East Jewett" }, { "East Marion", "East Marion" }, { "East Meadow", "East Meadow" }, { "East Meredith", "East Meredith" },
			    { "East Moriches", "East Moriches" }, { "East Nassau", "East Nassau" }, { "East Northport", "East Northport" }, { "East Norwich", "East Norwich" },
			    { "East Otto", "East Otto" }, { "East Palmyra", "East Palmyra" }, { "East Patchogue", "East Patchogue" }, { "East Pembroke", "East Pembroke" },
			    { "East Pharsalia", "East Pharsalia" }, { "East Quogue", "East Quogue" }, { "East Randolph", "East Randolph" }, { "East Rochester", "East Rochester" },
			    { "East Rockaway", "East Rockaway" }, { "East Schodack", "East Schodack" }, { "East Setauket", "East Setauket" }, { "East Springfield", "East Springfield" },
			    { "East Syracuse", "East Syracuse" }, { "East Williamson", "East Williamson" }, { "East Williston", "East Williston" }, { "East Windham", "East Windham" },
			    { "East Worcester", "East Worcester" }, { "East Yaphank", "East Yaphank" }, { "Eastchester", "Eastchester" }, { "Eastport", "Eastport" }, { "Eaton", "Eaton" },
			    { "Eddyville", "Eddyville" }, { "Eden", "Eden" }, { "Edgemere", "Edgemere" }, { "Edgewood", "Edgewood" }, { "Edmeston", "Edmeston" }, { "Edwards", "Edwards" },
			    { "Eggertsville", "Eggertsville" }, { "Elba", "Elba" }, { "Elbridge", "Elbridge" }, { "Eldred", "Eldred" }, { "Elizabethtown", "Elizabethtown" },
			    { "Elizaville", "Elizaville" }, { "Elka Park", "Elka Park" }, { "Ellenburg", "Ellenburg" }, { "Ellenburg Center", "Ellenburg Center" },
			    { "Ellenburg Depot", "Ellenburg Depot" }, { "Ellenville", "Ellenville" }, { "Ellicottville", "Ellicottville" }, { "Ellington", "Ellington" },
			    { "Ellisburg", "Ellisburg" }, { "Elma", "Elma" }, { "Elmhurst", "Elmhurst" }, { "Elmira", "Elmira" }, { "Elmira Heights", "Elmira Heights" },
			    { "Elmont", "Elmont" }, { "Elmsford", "Elmsford" }, { "Elwood", "Elwood" }, { "Endicott", "Endicott" }, { "Endwell", "Endwell" },
			    { "Erieville", "Erieville" }, { "Erin", "Erin" }, { "Esopus", "Esopus" }, { "Esperance", "Esperance" }, { "Essex", "Essex" },
			    { "Etna", "Etna" }, { "Evans Mills", "Evans Mills" }, { "Fabius", "Fabius" }, { "Fair Harbor", "Fair Harbor" }, { "Fair Haven",
			    "Fair Haven" }, { "Fairport", "Fairport" }, { "Falconer", "Falconer" }, { "Fallsburg", "Fallsburg" }, { "Fancher", "Fancher" },
			    { "Far Rockaway", "Far Rockaway" }, { "Farmersville Station", "Farmersville Station" }, { "Farmingdale", "Farmingdale" },
			    { "Farmington", "Farmington" }, { "Farmingville", "Farmingville" }, { "Farnham", "Farnham" }, { "Fayette", "Fayette" },
			    { "Fayetteville", "Fayetteville" }, { "Felts Mills", "Felts Mills" }, { "Ferndale", "Ferndale" }, { "Feura Bush", "Feura Bush" },
			    { "Fillmore", "Fillmore" }, { "Findley Lake", "Findley Lake" }, { "Fine", "Fine" }, { "Fineview", "Fineview" }, { "Fire Island Pines", "Fire Island Pines" },
			    { "Fishers", "Fishers" }, { "Fishers Island", "Fishers Island" }, { "Fishers Landing", "Fishers Landing" }, { "Fishkill", "Fishkill" },
			    { "Fishs Eddy", "Fishs Eddy" }, { "Flanders", "Flanders" }, { "Fleetwood", "Fleetwood" }, { "Fleischmanns", "Fleischmanns" }, 
			    { "Floral Park", "Floral Park" }, { "Florida", "Florida" }, { "Flushing", "Flushing" }, { "Fly Creek", "Fly Creek" }, { "Fonda", "Fonda" },
			    { "Forest Hills", "Forest Hills" }, { "Forestburgh", "Forestburgh" }, { "Forestport", "Forestport" }, { "Forestville", "Forestville" },
			    { "Fort Ann", "Fort Ann" }, { "Fort Covington", "Fort Covington" }, { "Fort Drum", "Fort Drum" }, { "Fort Edward", "Fort Edward" },
			    { "Fort Hamilton", "Fort Hamilton" }, { "Fort Hunter", "Fort Hunter" }, { "Fort Jackson", "Fort Jackson" }, { "Fort Johnson", "Fort Johnson" },
			    { "Fort Montgomery", "Fort Montgomery" }, { "Fort Plain", "Fort Plain" }, { "Fort Salonga", "Fort Salonga" }, { "Fort Tilden", "Fort Tilden" },
			    { "Frankfort", "Frankfort" }, { "Franklin", "Franklin" }, { "Franklin Springs", "Franklin Springs" }, { "Franklin Square", "Franklin Square" },
			    { "Franklinville", "Franklinville" }, { "Fredonia", "Fredonia" }, { "Freedom", "Freedom" }, { "Freehold", "Freehold" }, { "Freeport", "Freeport" },
			    { "Freeville", "Freeville" }, { "Fremont Center", "Fremont Center" }, { "Fresh Meadows", "Fresh Meadows" }, { "Frewsburg", "Frewsburg" },
			    { "Friendship", "Friendship" }, { "Frontenac", "Frontenac" }, { "Fulton", "Fulton" }, { "Fultonham", "Fultonham" }, { "Fultonville", "Fultonville" },
			    { "Gabriels", "Gabriels" }, { "Gainesville", "Gainesville" }, { "Gallupville", "Gallupville" }, { "Galway", "Galway" }, { "Gansevoort", "Gansevoort" },
			    { "Garden City", "Garden City" }, { "Garden City Park", "Garden City Park" }, { "Garden City South", "Garden City South" }, { "Gardiner", "Gardiner" },
			    { "Garnerville", "Garnerville" }, { "Garrattsville", "Garrattsville" }, { "Garrison", "Garrison" }, { "Gasport", "Gasport" }, { "Gates", "Gates" },
			    { "Geneseo", "Geneseo" }, { "Geneva", "Geneva" }, { "Genoa", "Genoa" }, { "Georgetown", "Georgetown" }, { "Germantown", "Germantown" }, { "Gerry", "Gerry" },
			    { "Getzville", "Getzville" }, { "Ghent", "Ghent" }, { "Gilbertsville", "Gilbertsville" }, { "Gilboa", "Gilboa" }, { "Gilgo Beach", "Gilgo Beach" },
			    { "Glasco", "Glasco" }, { "Glen Aubrey", "Glen Aubrey" }, { "Glen Cove", "Glen Cove" }, { "Glen Head", "Glen Head" }, { "Glen Oaks", "Glen Oaks" },
			    { "Glen Park", "Glen Park" }, { "Glen Spey", "Glen Spey" }, { "Glen Wild", "Glen Wild" }, { "Glendale", "Glendale" }, { "Glenfield", "Glenfield" },
			    { "Glenford", "Glenford" }, { "Glenham", "Glenham" }, { "Glenmont", "Glenmont" }, { "Glens Falls", "Glens Falls" }, { "Glenville", "Glenville" },
			    { "Glenwood", "Glenwood" }, { "Glenwood Landing", "Glenwood Landing" }, { "Gloversville", "Gloversville" }, { "Godeffroy", "Godeffroy" },
			    { "Goldens Brg", "Goldens Brg" }, { "Goldens Bridge", "Goldens Bridge" }, { "Gorham", "Gorham" }, { "Goshen", "Goshen" }, { "Gouverneur", "Gouverneur" },
			    { "Gowanda", "Gowanda" }, { "Grafton", "Grafton" }, { "Grahamsville", "Grahamsville" }, { "Grand Gorge", "Grand Gorge" },
			    { "Grand Island", "Grand Island" }, { "Grandview On Hudson", "Grandview On Hudson" }, { "Granite Springs", "Granite Springs" },
			    { "Granville", "Granville" }, { "Great Bend", "Great Bend" }, { "Great Neck", "Great Neck" }, { "Great Neck Plaza", "Great Neck Plaza" },
			    { "Great River", "Great River" }, { "Great Valley", "Great Valley" }, { "Greece", "Greece" }, { "Green Island", "Green Island" }, { "Greene", "Greene" },
			    { "Greenfield Center", "Greenfield Center" }, { "Greenfield Park", "Greenfield Park" }, { "Greenhurst", "Greenhurst" }, { "Greenlawn", "Greenlawn" },
			    { "Greenport", "Greenport" }, { "Greenvale", "Greenvale" }, { "Greenville", "Greenville" }, { "Greenwich", "Greenwich" }, { "Greenwood", "Greenwood" },
			    { "Greenwood Lake", "Greenwood Lake" }, { "Greig", "Greig" }, { "Grenell", "Grenell" }, { "Groton", "Groton" }, { "Groveland", "Groveland" },
			    { "Guilderland", "Guilderland" }, { "Guilderland Center", "Guilderland Center" }, { "Guilford", "Guilford" }, { "Hadley", "Hadley" }, { "Hagaman",
			    "Hagaman" }, { "Hague", "Hague" }, { "Hailesboro", "Hailesboro" }, { "Haines Falls", "Haines Falls" }, { "Halcott Center", "Halcott Center" },
			    { "Halcottsville", "Halcottsville" }, { "Halesite", "Halesite" }, { "Halfmoon", "Halfmoon" }, { "Hall", "Hall" }, { "Hamburg", "Hamburg" },
			    { "Hamden", "Hamden" }, { "Hamilton", "Hamilton" }, { "Hamlin", "Hamlin" }, { "Hammond", "Hammond" }, { "Hammondsport", "Hammondsport" },
			    { "Hampton", "Hampton" }, { "Hampton Bays", "Hampton Bays" }, { "Hancock", "Hancock" }, { "Hankins", "Hankins" }, { "Hannacroix", "Hannacroix" },
			    { "Hannawa Falls", "Hannawa Falls" }, { "Hannibal", "Hannibal" }, { "Harford", "Harford" }, { "Harford Mills", "Harford Mills" },
			    { "Harpersfield", "Harpersfield" }, { "Harpursville", "Harpursville" }, { "Harriman", "Harriman" }, { "Harris", "Harris" }, { "Harrison", "Harrison" },
			    { "Harrisville", "Harrisville" }, { "Hartford", "Hartford" }, { "Hartsdale", "Hartsdale" }, { "Hartwick", "Hartwick" },
			    { "Hartwick Seminary", "Hartwick Seminary" }, { "Hastings", "Hastings" }, { "Hastings On Hudson", "Hastings On Hudson" },
			    { "Hauppauge", "Hauppauge" }, { "Haverstraw", "Haverstraw" }, { "Hawthorne", "Hawthorne" }, { "Hayt Corners", "Hayt Corners" },
			    { "Heathcote", "Heathcote" }, { "Hector", "Hector" }, { "Helena", "Helena" }, { "Hemlock", "Hemlock" }, { "Hempstead", "Hempstead" },
			    { "Henderson", "Henderson" }, { "Henderson Harbor", "Henderson Harbor" }, { "Henrietta", "Henrietta" }, { "Hensonville", "Hensonville" },
			    { "Herkimer", "Herkimer" }, { "Hermon", "Hermon" }, { "Heuvelton", "Heuvelton" }, { "Hewlett", "Hewlett" }, { "Hicksville", "Hicksville" },
			    { "High Falls", "High Falls" }, { "Highland", "Highland" }, { "Highland Falls", "Highland Falls" }, { "Highland Lake", "Highland Lake" },
			    { "Highland Mills", "Highland Mills" }, { "Highmount", "Highmount" }, { "Hillburn", "Hillburn" }, { "Hillsdale", "Hillsdale" },
			    { "Hillside Manor", "Hillside Manor" }, { "Hilton", "Hilton" }, { "Himrod", "Himrod" }, { "Hinckley", "Hinckley" }, { "Hinsdale", "Hinsdale" },
			    { "Hobart", "Hobart" }, { "Hoffmeister", "Hoffmeister" }, { "Hofstra University", "Hofstra University" }, { "Hogansburg", "Hogansburg" },
			    { "Holbrook", "Holbrook" }, { "Holland", "Holland" }, { "Holland Patent", "Holland Patent" }, { "Holley", "Holley" }, { "Hollis", "Hollis" },
			    { "Hollis Hills", "Hollis Hills" }, { "Hollowville", "Hollowville" }, { "Holmes", "Holmes" }, { "Holtsville", "Holtsville" }, { "Homer", "Homer" },
			    { "Honeoye", "Honeoye" }, { "Honeoye Falls", "Honeoye Falls" }, { "Hoosick", "Hoosick" }, { "Hoosick Falls", "Hoosick Falls" }, { "Hopewell", "Hopewell" },
			    { "Hopewell Junction", "Hopewell Junction" }, { "Hopkinton", "Hopkinton" }, { "Hornell", "Hornell" }, { "Horseheads", "Horseheads" },
			    { "Hortonville", "Hortonville" }, { "Houghton", "Houghton" }, { "Howard Beach", "Howard Beach" }, { "Howells", "Howells" },
			    { "Howes Cave", "Howes Cave" }, { "Hubbardsville", "Hubbardsville" }, { "Hudson", "Hudson" }, { "Hudson Falls", "Hudson Falls" },
			    { "Hughsonville", "Hughsonville" }, { "Huguenot", "Huguenot" }, { "Hulberton", "Hulberton" }, { "Huletts Landing", "Huletts Landing" },
			    { "Hume", "Hume" }, { "Hunt", "Hunt" }, { "Hunter", "Hunter" }, { "Huntington", "Huntington" }, { "Huntington Station", "Huntington Station" },
			    { "Hurley", "Hurley" }, { "Hurleyville", "Hurleyville" }, { "Hyde Park", "Hyde Park" }, { "Ilion", "Ilion" }, { "Indian Lake", "Indian Lake" },
			    { "Industry", "Industry" }, { "Inlet", "Inlet" }, { "Interlaken", "Interlaken" }, { "Inwood", "Inwood" }, { "Ionia", "Ionia" }, { "Irondequoit",
			    "Irondequoit" }, { "Irving", "Irving" }, { "Irvington", "Irvington" }, { "Ischua", "Ischua" }, { "Island Park", "Island Park" }, { "Islandia", "Islandia" },
			    { "Islip", "Islip" }, { "Islip Terrace", "Islip Terrace" }, { "Ithaca", "Ithaca" }, { "Jackson Heights", "Jackson Heights" }, { "Jacksonville", "Jacksonville" },
			    { "Jamaica", "Jamaica" }, { "Jamesport", "Jamesport" }, { "Jamestown", "Jamestown" }, { "Jamesville", "Jamesville" }, { "Jasper", "Jasper" },
			    { "Java Center", "Java Center" }, { "Java Village", "Java Village" }, { "Jay", "Jay" }, { "Jefferson", "Jefferson" },
			    { "Jefferson Valley", "Jefferson Valley" }, { "Jeffersonville", "Jeffersonville" }, { "Jericho", "Jericho" }, { "Jewett", "Jewett" },
			    { "Jfk Airport", "Jfk Airport" }, { "John F Kennedy Airport", "John F Kennedy Airport" }, { "Johnsburg", "Johnsburg" }, { "Johnson", "Johnson" },
			    { "Johnson City", "Johnson City" }, { "Johnsonville", "Johnsonville" }, { "Johnstown", "Johnstown" }, { "Jordan", "Jordan" }, { "Jordanville", "Jordanville" },
			    { "Kanona", "Kanona" }, { "Kaser", "Kaser" }, { "Katonah", "Katonah" }, { "Kattskill Bay", "Kattskill Bay" }, { "Kauneonga Lake", "Kauneonga Lake" },
			    { "Keene", "Keene" }, { "Keene Valley", "Keene Valley" }, { "Keeseville", "Keeseville" }, { "Kendall", "Kendall" }, { "Kenmore", "Kenmore" },
			    { "Kennedy", "Kennedy" }, { "Kenoza Lake", "Kenoza Lake" }, { "Kent", "Kent" }, { "Kent Cliffs", "Kent Cliffs" }, { "Kent Lakes", "Kent Lakes" },
			    { "Kerhonkson", "Kerhonkson" }, { "Keuka Park", "Keuka Park" }, { "Kew Gardens", "Kew Gardens" }, { "Kew Gardens Hills", "Kew Gardens Hills" },
			    { "Kiamesha Lake", "Kiamesha Lake" }, { "Kill Buck", "Kill Buck" }, { "Killawog", "Killawog" }, { "Kinderhook", "Kinderhook" },
			    { "King Ferry", "King Ferry" }, { "Kings Park", "Kings Park" }, { "Kings Point", "Kings Point" }, { "Kingston", "Kingston" }, { "Kirkville", "Kirkville" },
			    { "Kirkwood", "Kirkwood" }, { "Kiryas Joel", "Kiryas Joel" }, { "Kismet", "Kismet" }, { "Knapp Creek", "Knapp Creek" }, { "Knowlesville", "Knowlesville" },
			    { "Knox", "Knox" }, { "Knoxboro", "Knoxboro" }, { "Krumville", "Krumville" }, { "La Fargeville", "La Fargeville" }, { "La Fayette", "La Fayette" },
			    { "La Guardia Airport", "La Guardia Airport" }, { "Lackawanna", "Lackawanna" }, { "Lacona", "Lacona" }, { "Lagrangeville", "Lagrangeville" },
			    { "Lake Clear", "Lake Clear" }, { "Lake George", "Lake George" }, { "Lake Grove", "Lake Grove" }, { "Lake Hill", "Lake Hill" },
			    { "Lake Huntington", "Lake Huntington" }, { "Lake Katrine", "Lake Katrine" }, { "Lake Lincolndale", "Lake Lincolndale" }, { "Lake Luzerne", "Lake Luzerne" },
			    { "Lake Peekskill", "Lake Peekskill" }, { "Lake Placid", "Lake Placid" }, { "Lake Pleasant", "Lake Pleasant" }, { "Lake Ronkonkoma", "Lake Ronkonkoma" },
			    { "Lake Success", "Lake Success" }, { "Lake View", "Lake View" }, { "Lakemont", "Lakemont" }, { "Lakeville", "Lakeville" }, { "Lakewood", "Lakewood" },
			    { "Lancaster", "Lancaster" }, { "Lanesville", "Lanesville" }, { "Lansing", "Lansing" }, { "Larchmont", "Larchmont" }, { "Latham", "Latham" },
			    { "Laurel", "Laurel" }, { "Laurelton", "Laurelton" }, { "Laurens", "Laurens" }, { "Lawrence", "Lawrence" }, { "Lawrenceville", "Lawrenceville" },
			    { "Lawtons", "Lawtons" }, { "Lawyersville", "Lawyersville" }, { "Le Roy", "Le Roy" }, { "Lebanon", "Lebanon" }, { "Lebanon Springs", "Lebanon Springs" },
			    { "Lee Center", "Lee Center" }, { "Leeds", "Leeds" }, { "Leicester", "Leicester" }, { "Leon", "Leon" }, { "Leonardsville", "Leonardsville" },
			    { "Levittown", "Levittown" }, { "Lew Beach", "Lew Beach" }, { "Lewis", "Lewis" }, { "Lewiston", "Lewiston" }, { "Lexington", "Lexington" }, 
			    { "Liberty", "Liberty" }, { "Lido Beach", "Lido Beach" }, { "Lily Dale", "Lily Dale" }, { "Lima", "Lima" }, { "Limerick", "Limerick" },
			    { "Limestone", "Limestone" }, { "Lincolndale", "Lincolndale" }, { "Lindenhurst", "Lindenhurst" }, { "Lindley", "Lindley" }, { "Linwood", "Linwood" },
			    { "Lisbon", "Lisbon" }, { "Lisle", "Lisle" }, { "Little Falls", "Little Falls" }, { "Little Genesee", "Little Genesee" }, { "Little Neck", "Little Neck" },
			    { "Little Valley", "Little Valley" }, { "Little York", "Little York" }, { "Liverpool", "Liverpool" }, { "Livingston", "Livingston" }, 
			    { "Livingston Manor", "Livingston Manor" }, { "Livonia", "Livonia" }, { "Livonia Center", "Livonia Center" }, { "Lloyd Harbor", "Lloyd Harbor" },
			    { "Loch Sheldrake", "Loch Sheldrake" }, { "Locke", "Locke" }, { "Lockport", "Lockport" }, { "Lockwood", "Lockwood" }, { "Locust Valley", "Locust Valley" },
			    { "Lodi", "Lodi" }, { "Loehmanns Plaza", "Loehmanns Plaza" }, { "Long Beach", "Long Beach" }, { "Long Eddy", "Long Eddy" }, 
			    { "Long Island City", "Long Island City" }, { "Long Lake", "Long Lake" }, { "Loon Lake", "Loon Lake" }, { "Lorraine", "Lorraine" },
			    { "Loudonville", "Loudonville" }, { "Lowman", "Lowman" }, { "Lowville", "Lowville" }, { "Lycoming", "Lycoming" }, { "Lynbrook", "Lynbrook" }, 
			    { "Lyndonville", "Lyndonville" }, { "Lyon Mountain", "Lyon Mountain" }, { "Lyons", "Lyons" }, { "Lyons Falls", "Lyons Falls" }, { "Lysander", "Lysander" },
			    { "Mac Dougall", "Mac Dougall" }, { "Macedon", "Macedon" }, { "Machias", "Machias" }, { "Madison", "Madison" }, { "Madrid", "Madrid" }, { "Mahopac", "Mahopac" },
			    { "Mahopac Falls", "Mahopac Falls" }, { "Maine", "Maine" }, { "Malba", "Malba" }, { "Malden Bridge", "Malden Bridge" }, { "Malden Hudson", "Malden Hudson" },
			    { "Malden On Hudson", "Malden On Hudson" }, { "Mallory", "Mallory" }, { "Malone", "Malone" }, { "Malta", "Malta" }, { "Malverne", "Malverne" },
			    { "Mamaroneck", "Mamaroneck" }, { "Manchester", "Manchester" }, { "Manhasset", "Manhasset" }, { "Manhasset Hills", "Manhasset Hills" },
			    { "Manlius", "Manlius" }, { "Mannsville", "Mannsville" }, { "Manorville", "Manorville" }, { "Maple Springs", "Maple Springs" }, 
			    { "Maple View", "Maple View" }, { "Maplecrest", "Maplecrest" }, { "Marathon", "Marathon" }, { "Marcellus", "Marcellus" }, { "Marcy", "Marcy" },
			    { "Margaretville", "Margaretville" }, { "Marietta", "Marietta" }, { "Marilla", "Marilla" }, { "Marion", "Marion" }, { "Marlboro", "Marlboro" },
			    { "Martinsburg", "Martinsburg" }, { "Martville", "Martville" }, { "Maryknoll", "Maryknoll" }, { "Maryland", "Maryland" }, { "Masonville", "Masonville" },
			    { "Maspeth", "Maspeth" }, { "Massapequa", "Massapequa" }, { "Massapequa Park", "Massapequa Park" }, { "Massawepie", "Massawepie" },
			    { "Massena", "Massena" }, { "Mastic", "Mastic" }, { "Mastic Beach", "Mastic Beach" }, { "Mattituck", "Mattituck" }, { "Mattydale", "Mattydale" },
			    { "Maybrook", "Maybrook" }, { "Mayfield", "Mayfield" }, { "Mayville", "Mayville" }, { "Mcconnellsville", "Mcconnellsville" }, { "Mcdonough", "Mcdonough" },
			    { "Mcgraw", "Mcgraw" }, { "Mclean", "Mclean" }, { "Meacham", "Meacham" }, { "Mechanicville", "Mechanicville" }, { "Mecklenburg", "Mecklenburg" },
			    { "Medford", "Medford" }, { "Medina", "Medina" }, { "Medusa", "Medusa" }, { "Mellenville", "Mellenville" }, { "Melrose", "Melrose" },
			    { "Melville", "Melville" }, { "Memphis", "Memphis" }, { "Menands", "Menands" }, { "Mendon", "Mendon" }, { "Meredith", "Meredith" },
			    { "Meridale", "Meridale" }, { "Meridian", "Meridian" }, { "Merrick", "Merrick" }, { "Merrill", "Merrill" }, { "Mexico", "Mexico" },
			    { "Mid Hudson", "Mid Hudson" }, { "Middle Falls", "Middle Falls" }, { "Middle Granville", "Middle Granville" }, { "Middle Grove", "Middle Grove" },
			    { "Middle Island", "Middle Island" }, { "Middle Village", "Middle Village" }, { "Middleburgh", "Middleburgh" }, { "Middleport", "Middleport" },
			    { "Middlesex", "Middlesex" }, { "Middletown", "Middletown" }, { "Middleville", "Middleville" }, { "Milan", "Milan" }, { "Milford", "Milford" },
			    { "Mill Neck", "Mill Neck" }, { "Millbrook", "Millbrook" }, { "Miller Place", "Miller Place" }, { "Millerton", "Millerton" }, { "Millport", "Millport" },
			    { "Millwood", "Millwood" }, { "Milton", "Milton" }, { "Mineola", "Mineola" }, { "Minerva", "Minerva" }, { "Minetto", "Minetto" }, 
			    { "Mineville", "Mineville" }, { "Minoa", "Minoa" }, { "Model City", "Model City" }, { "Modena", "Modena" }, { "Mohawk", "Mohawk" },
			    { "Mohegan Lake", "Mohegan Lake" }, { "Moira", "Moira" }, { "Mongaup Valley", "Mongaup Valley" }, { "Monroe", "Monroe" }, 
			    { "Monsey", "Monsey" }, { "Montauk", "Montauk" }, { "Montebello", "Montebello" }, { "Montezuma", "Montezuma" }, { "Montgomery", "Montgomery" }, 
			    { "Monticello", "Monticello" }, { "Montour Falls", "Montour Falls" }, { "Montrose", "Montrose" }, { "Mooers", "Mooers" }, { "Mooers Forks", "Mooers Forks" },
			    { "Moravia", "Moravia" }, { "Moriah", "Moriah" }, { "Moriah Center", "Moriah Center" }, { "Moriches", "Moriches" }, { "Morris", "Morris" },
			    { "Morrisonville", "Morrisonville" }, { "Morristown", "Morristown" }, { "Morrisville", "Morrisville" }, { "Morton", "Morton" },
			    { "Mottville", "Mottville" }, { "Mount Kisco", "Mount Kisco" }, { "Mount Marion", "Mount Marion" }, { "Mount Morris", "Mount Morris" },
			    { "Mount Sinai", "Mount Sinai" }, { "Mount Tremper", "Mount Tremper" }, { "Mount Upton", "Mount Upton" }, { "Mount Vernon", "Mount Vernon" },
			    { "Mount Vision", "Mount Vision" }, { "Mountain Dale", "Mountain Dale" }, { "Mountainville", "Mountainville" }, { "Mumford", "Mumford" },
			    { "Munnsville", "Munnsville" }, { "Murray Isle", "Murray Isle" }, { "Nanuet", "Nanuet" }, { "Napanoch", "Napanoch" }, { "Naples", "Naples" },
			    { "Narrowsburg", "Narrowsburg" }, { "Nassau", "Nassau" }, { "Natural Bridge", "Natural Bridge" }, { "Nedrow", "Nedrow" }, { "Nelliston", "Nelliston" },
			    { "Nelsonville", "Nelsonville" }, { "Neponsit", "Neponsit" }, { "Nesconset", "Nesconset" }, { "Neversink", "Neversink" },
			    { "New Baltimore", "New Baltimore" }, { "New Berlin", "New Berlin" }, { "New City", "New City" }, { "New Hamburg", "New Hamburg" },
			    { "New Hampton", "New Hampton" }, { "New Hartford", "New Hartford" }, { "New Haven", "New Haven" }, { "New Hyde Park", "New Hyde Park" },
			    { "New Kingston", "New Kingston" }, { "New Lebanon", "New Lebanon" }, { "New Lisbon", "New Lisbon" }, { "New Milford", "New Milford" }, 
			    { "New Paltz", "New Paltz" }, { "New Rochelle", "New Rochelle" }, { "New Russia", "New Russia" }, { "New Square", "New Square" },
			    { "New Suffolk", "New Suffolk" }, { "New Windsor", "New Windsor" }, { "New Woodstock", "New Woodstock" }, { "New York", "New York" },
			    { "New York Mills", "New York Mills" }, { "Newark", "Newark" }, { "Newark Valley", "Newark Valley" }, { "Newburgh", "Newburgh" }, 
			    { "Newcomb", "Newcomb" }, { "Newfane", "Newfane" }, { "Newfield", "Newfield" }, { "Newport", "Newport" }, { "Newton Falls", "Newton Falls" },
			    { "Newtonville", "Newtonville" }, { "Niagara Falls", "Niagara Falls" }, { "Niagara University", "Niagara University" }, { "Nichols", "Nichols" },
			    { "Nicholville", "Nicholville" }, { "Nineveh", "Nineveh" }, { "Niobe", "Niobe" }, { "Niskayuna", "Niskayuna" }, { "Niverville", "Niverville" },
			    { "Norfolk", "Norfolk" }, { "North Babylon", "North Babylon" }, { "North Baldwin", "North Baldwin" }, { "North Bangor", "North Bangor" },
			    { "North Bay", "North Bay" }, { "North Bellmore", "North Bellmore" }, { "North Blenheim", "North Blenheim" }, { "North Boston", "North Boston" },
			    { "North Branch", "North Branch" }, { "North Brookfield", "North Brookfield" }, { "North Castle", "North Castle" }, { "North Chatham", "North Chatham" },
			    { "North Chili", "North Chili" }, { "North Cohocton", "North Cohocton" }, { "North Collins", "North Collins" }, { "North Creek", "North Creek" },
			    { "North Evans", "North Evans" }, { "North Granville", "North Granville" }, { "North Greece", "North Greece" }, { "North Hills", "North Hills" }, 
			    { "North Hoosick", "North Hoosick" }, { "North Hornell", "North Hornell" }, { "North Hudson", "North Hudson" }, { "North Java", "North Java" },
			    { "North Lawrence", "North Lawrence" }, { "North Massapequa", "North Massapequa" }, { "North Merrick", "North Merrick" }, 
			    { "North New Hyde Park", "North New Hyde Park" }, { "North Norwich", "North Norwich" }, { "North Pitcher", "North Pitcher" },
			    { "North River", "North River" }, { "North Rose", "North Rose" }, { "North Salem", "North Salem" }, { "North Syracuse", "North Syracuse" },
			    { "North Tarrytown", "North Tarrytown" }, { "North Tonawanda", "North Tonawanda" }, { "Northport", "Northport" }, { "Northville", "Northville" },
			    { "Norton Hill", "Norton Hill" }, { "Norwich", "Norwich" }, { "Norwood", "Norwood" }, { "Nunda", "Nunda" }, { "Nyack", "Nyack" }, 
			    { "Oak Beach", "Oak Beach" }, { "Oak Hill", "Oak Hill" }, { "Oak Island", "Oak Island" }, { "Oakdale", "Oakdale" },
			    { "Oakfield", "Oakfield" }, { "Oakland Gardens", "Oakland Gardens" }, { "Oaks Corners", "Oaks Corners" }, { "Obernburg", "Obernburg" }, 
			    { "Ocean Beach", "Ocean Beach" }, { "Oceanside", "Oceanside" }, { "Odessa", "Odessa" }, { "Ogdensburg", "Ogdensburg" }, { "Ohio", "Ohio" },
			    { "Olcott", "Olcott" }, { "Old Bethpage", "Old Bethpage" }, { "Old Chatham", "Old Chatham" }, { "Old Forge", "Old Forge" }, { "Old Westbury", "Old Westbury" },
			    { "Olean", "Olean" }, { "Olivebridge", "Olivebridge" }, { "Oliverea", "Oliverea" }, { "Olmstedville", "Olmstedville" }, 
			    { "Onchiota", "Onchiota" }, { "Oneida", "Oneida" }, { "Oneonta", "Oneonta" }, { "Ontario", "Ontario" }, { "Ontario Center", "Ontario Center" },
			    { "Orangeburg", "Orangeburg" }, { "Orchard Park", "Orchard Park" }, { "Orient", "Orient" }, { "Oriskany", "Oriskany" }, { "Oriskany Falls", "Oriskany Falls" },
			    { "Orwell", "Orwell" }, { "Ossining", "Ossining" }, { "Oswegatchie", "Oswegatchie" }, { "Oswego", "Oswego" }, { "Otego", "Otego" },
			    { "Otisville", "Otisville" }, { "Otto", "Otto" }, { "Ouaquaga", "Ouaquaga" }, { "Ovid", "Ovid" }, { "Owasco", "Owasco" },
			    { "Owego", "Owego" }, { "Owls Head", "Owls Head" }, { "Oxbow", "Oxbow" }, { "Oxford", "Oxford" }, { "Oyster Bay", "Oyster Bay" }, 
			    { "Ozone Park", "Ozone Park" }, { "Painted Post", "Painted Post" }, { "Palatine Bridge", "Palatine Bridge" }, { "Palenville", "Palenville" },
			    { "Palisades", "Palisades" }, { "Palmyra", "Palmyra" }, { "Panama", "Panama" }, { "Panorama", "Panorama" }, { "Paradox", "Paradox" },
			    { "Paris", "Paris" }, { "Parish", "Parish" }, { "Parishville", "Parishville" }, { "Parksville", "Parksville" }, { "Patchogue", "Patchogue" },
			    { "Patterson", "Patterson" }, { "Pattersonville", "Pattersonville" }, { "Paul Smiths", "Paul Smiths" }, { "Pavilion", "Pavilion" }, 
			    { "Pawling", "Pawling" }, { "Pearl River", "Pearl River" }, { "Peconic", "Peconic" }, { "Peekskill", "Peekskill" }, { "Pelham", "Pelham" },
			    { "Penfield", "Penfield" }, { "Penn Yan", "Penn Yan" }, { "Pennellville", "Pennellville" }, { "Perkinsville", "Perkinsville" }, { "Perry", "Perry" },
			    { "Perrysburg", "Perrysburg" }, { "Perryville", "Perryville" }, { "Peru", "Peru" }, { "Peterboro", "Peterboro" }, { "Petersburg", "Petersburg" },
			    { "Petersburgh", "Petersburgh" }, { "Phelps", "Phelps" }, { "Philadelphia", "Philadelphia" }, { "Phillipsport", "Phillipsport" }, { "Philmont", "Philmont" },
			    { "Phoenicia", "Phoenicia" }, { "Phoenix", "Phoenix" }, { "Piercefield", "Piercefield" }, { "Piermont", "Piermont" }, 
			    { "Pierrepont Manor", "Pierrepont Manor" }, { "Piffard", "Piffard" }, { "Pike", "Pike" }, { "Pilot Knob", "Pilot Knob" }, 
			    { "Pine Bush", "Pine Bush" }, { "Pine City", "Pine City" }, { "Pine Hill", "Pine Hill" }, { "Pine Island", "Pine Island" }, 
			    { "Pine Plains", "Pine Plains" }, { "Pine Valley", "Pine Valley" }, { "Piseco", "Piseco" }, { "Pitcher", "Pitcher" }, 
			    { "Pittsford", "Pittsford" }, { "Plainview", "Plainview" }, { "Plainville", "Plainville" }, { "Plandome", "Plandome" }, 
			    { "Plattekill", "Plattekill" }, { "Plattsburgh", "Plattsburgh" }, { "Pleasant Valley", "Pleasant Valley" }, { "Pleasantville", "Pleasantville" },
			    { "Plessis", "Plessis" }, { "Plymouth", "Plymouth" }, { "Poestenkill", "Poestenkill" }, { "Point Lookout", "Point Lookout" }, 
			    { "Point O Woods", "Point O Woods" }, { "Point Vivian", "Point Vivian" }, { "Poland", "Poland" }, { "Pomona", "Pomona" }, 
			    { "Pompey", "Pompey" }, { "Pond Eddy", "Pond Eddy" }, { "Poolville", "Poolville" }, { "Poplar Ridge", "Poplar Ridge" },
			    { "Port Byron", "Port Byron" }, { "Port Chester", "Port Chester" }, { "Port Crane", "Port Crane" }, { "Port Ewen", "Port Ewen" }, 
			    { "Port Gibson", "Port Gibson" }, { "Port Henry", "Port Henry" }, { "Port Jefferson", "Port Jefferson" }, 
			    { "Port Jefferson Station", "Port Jefferson Station" }, { "Port Jervis", "Port Jervis" }, { "Port Kent", "Port Kent" },
			    { "Port Leyden", "Port Leyden" }, { "Port Washington", "Port Washington" }, { "Portageville", "Portageville" }, { "Porter Corners", "Porter Corners" },
			    { "Portland", "Portland" }, { "Portlandville", "Portlandville" }, { "Portville", "Portville" }, { "Potsdam", "Potsdam" }, { "Pottersville", "Pottersville" },
			    { "Poughkeepsie", "Poughkeepsie" }, { "Poughquag", "Poughquag" }, { "Pound Ridge", "Pound Ridge" }, { "Pratts Hollow", "Pratts Hollow" }, 
			    { "Prattsburgh", "Prattsburgh" }, { "Prattsville", "Prattsville" }, { "Preble", "Preble" }, { "Preston Hollow", "Preston Hollow" },
			    { "Prospect", "Prospect" }, { "Pulaski", "Pulaski" }, { "Pulteney", "Pulteney" }, { "Pultneyville", "Pultneyville" }, { "Purchase", "Purchase" },
			    { "Purdys", "Purdys" }, { "Purling", "Purling" }, { "Putnam Station", "Putnam Station" }, { "Putnam Valley", "Putnam Valley" },
			    { "Pyrites", "Pyrites" }, { "Quaker Street", "Quaker Street" }, { "Queens Village", "Queens Village" }, { "Queensbury", "Queensbury" },
			    { "Quogue", "Quogue" }, { "Rainbow Lake", "Rainbow Lake" }, { "Randolph", "Randolph" }, { "Ransomville", "Ransomville" },
			    { "Raquette Lake", "Raquette Lake" }, { "Ravena", "Ravena" }, { "Ray Brook", "Ray Brook" }, { "Raymondville", "Raymondville" },
			    { "Reading Center", "Reading Center" }, { "Red Creek", "Red Creek" }, { "Red Hook", "Red Hook" }, { "Redfield", "Redfield" },
			    { "Redford", "Redford" }, { "Redwood", "Redwood" }, { "Rego Park", "Rego Park" }, { "Remsen", "Remsen" }, { "Remsenburg", "Remsenburg" },
			    { "Rensselaer", "Rensselaer" }, { "Rensselaer Falls", "Rensselaer Falls" }, { "Rensselaerville", "Rensselaerville" }, { "Retsof", "Retsof" },
			    { "Rexford", "Rexford" }, { "Rexville", "Rexville" }, { "Rhinebeck", "Rhinebeck" }, { "Rhinecliff", "Rhinecliff" }, { "Richburg", "Richburg" },
			    { "Richfield Springs", "Richfield Springs" }, { "Richford", "Richford" }, { "Richland", "Richland" }, { "Richmond Hill", "Richmond Hill" },
			    { "Richmondville", "Richmondville" }, { "Richville", "Richville" }, { "Ridge", "Ridge" }, { "Ridgemont", "Ridgemont" }, { "Ridgewood", "Ridgewood" },
			    { "Rifton", "Rifton" }, { "Riparius", "Riparius" }, { "Ripley", "Ripley" }, { "Riverhead", "Riverhead" }, { "Rochdale Village", "Rochdale Village" },
			    { "Rochester", "Rochester" }, { "Rock City Falls", "Rock City Falls" }, { "Rock Glen", "Rock Glen" }, { "Rock Hill", "Rock Hill" },
			    { "Rock Stream", "Rock Stream" }, { "Rock Tavern", "Rock Tavern" }, { "Rockaway Beach", "Rockaway Beach" }, { "Rockaway Park", "Rockaway Park" },
			    { "Rockaway Point", "Rockaway Point" }, { "Rockville Centre", "Rockville Centre" }, { "Rocky Point", "Rocky Point" }, { "Rodman", "Rodman" },
			    { "Roessleville", "Roessleville" }, { "Rome", "Rome" }, { "Romulus", "Romulus" }, { "Ronkonkoma", "Ronkonkoma" }, { "Roosevelt", "Roosevelt" },
			    { "Roosevelt Island", "Roosevelt Island" }, { "Rooseveltown", "Rooseveltown" }, { "Roscoe", "Roscoe" }, { "Rose", "Rose" }, { "Roseboom", "Roseboom" },
			    { "Rosedale", "Rosedale" }, { "Rosendale", "Rosendale" }, { "Roslyn", "Roslyn" }, { "Roslyn Heights", "Roslyn Heights" }, { "Rossburg", "Rossburg" },
			    { "Rotterdam", "Rotterdam" }, { "Rotterdam Junction", "Rotterdam Junction" }, { "Round Lake", "Round Lake" }, { "Round Top", "Round Top" },
			    { "Rouses Point", "Rouses Point" }, { "Roxbury", "Roxbury" }, { "Ruby", "Ruby" }, { "Rush", "Rush" }, { "Rushford", "Rushford" },
			    { "Rushville", "Rushville" }, { "Russell", "Russell" }, { "Rye", "Rye" }, { "Rye Brook", "Rye Brook" }, { "Sabael", "Sabael" },
			    { "Sackets Harbor", "Sackets Harbor" }, { "Sag Harbor", "Sag Harbor" }, { "Sagaponack", "Sagaponack" }, { "Saint Albans", "Saint Albans" },
			    { "Saint Bonaventure", "Saint Bonaventure" }, { "Saint Huberts", "Saint Huberts" }, { "Saint James", "Saint James" }, 
			    { "Saint Johnsville", "Saint Johnsville" }, { "Saint Regis Falls", "Saint Regis Falls" }, { "Saint Remy", "Saint Remy" }, 
			    { "Salamanca", "Salamanca" }, { "Salem", "Salem" }, { "Salisbury Center", "Salisbury Center" }, { "Salisbury Mills", "Salisbury Mills" },
			    { "Salt Point", "Salt Point" }, { "Saltaire", "Saltaire" }, { "Sanborn", "Sanborn" }, { "Sand Lake", "Sand Lake" }, { "Sands Point", "Sands Point" },
			    { "Sandusky", "Sandusky" }, { "Sandy Creek", "Sandy Creek" }, { "Sangerfield", "Sangerfield" }, { "Sanitaria Springs", "Sanitaria Springs" },
			    { "Saranac", "Saranac" }, { "Saranac Lake", "Saranac Lake" }, { "Saratoga Springs", "Saratoga Springs" }, { "Sardinia", "Sardinia" }, 
			    { "Saugerties", "Saugerties" }, { "Sauquoit", "Sauquoit" }, { "Savannah", "Savannah" }, { "Savona", "Savona" }, { "Sayville", "Sayville" },
			    { "Scarborough", "Scarborough" }, { "Scarsdale", "Scarsdale" }, { "Schaghticoke", "Schaghticoke" }, { "Schenectady", "Schenectady" }, 
			    { "Schenevus", "Schenevus" }, { "Schodack Landing", "Schodack Landing" }, { "Schoharie", "Schoharie" }, { "Schroon Lake", "Schroon Lake" },
			    { "Schuyler", "Schuyler" }, { "Schuyler Falls", "Schuyler Falls" }, { "Schuyler Lake", "Schuyler Lake" }, { "Schuylerville", "Schuylerville" },
			    { "Scio", "Scio" }, { "Scipio Center", "Scipio Center" }, { "Scotchtown", "Scotchtown" }, { "Scotia", "Scotia" }, { "Scottsburg", "Scottsburg" },
			    { "Scottsville", "Scottsville" }, { "Sea Cliff", "Sea Cliff" }, { "Seaford", "Seaford" }, { "Selden", "Selden" }, { "Selkirk", "Selkirk" },
			    { "Seneca Castle", "Seneca Castle" }, { "Seneca Falls", "Seneca Falls" }, { "Setauket", "Setauket" }, { "Severance", "Severance" }, { "Shady", "Shady" },
			    { "Shandaken", "Shandaken" }, { "Sharon Springs", "Sharon Springs" }, { "Shelter Island", "Shelter Island" }, 
			    { "Shelter Island Heights", "Shelter Island Heights" }, { "Shenorock", "Shenorock" }, { "Sherburne", "Sherburne" }, { "Sheridan", "Sheridan" },
			    { "Sherman", "Sherman" }, { "Sherrill", "Sherrill" }, { "Shinhopple", "Shinhopple" }, { "Shirley", "Shirley" }, { "Shokan", "Shokan" },
			    { "Shoreham", "Shoreham" }, { "Shortsville", "Shortsville" }, { "Shrub Oak", "Shrub Oak" }, { "Shushan", "Shushan" }, { "Sidney", "Sidney" },
			    { "Sidney Center", "Sidney Center" }, { "Siena", "Siena" }, { "Silver Bay", "Silver Bay" }, { "Silver Creek", "Silver Creek" },
			    { "Silver Lake", "Silver Lake" }, { "Silver Springs", "Silver Springs" }, { "Sinclairville", "Sinclairville" }, { "Skaneateles", "Skaneateles" },
			    { "Skaneateles Falls", "Skaneateles Falls" }, { "Slate Hill", "Slate Hill" }, { "Slaterville Springs", "Slaterville Springs" },
			    { "Sleepy Hollow", "Sleepy Hollow" }, { "Slingerlands", "Slingerlands" }, { "Sloan", "Sloan" }, { "Sloansville", "Sloansville" }, 
			    { "Sloatsburg", "Sloatsburg" }, { "Smallwood", "Smallwood" }, { "Smith Point", "Smith Point" }, { "Smithboro", "Smithboro" }, 
			    { "Smithtown", "Smithtown" }, { "Smithville", "Smithville" }, { "Smithville Flats", "Smithville Flats" }, { "Smyrna", "Smyrna" }, 
			    { "Snyder", "Snyder" }, { "Sodus", "Sodus" }, { "Sodus Center", "Sodus Center" }, { "Sodus Point", "Sodus Point" }, { "Solsville", "Solsville" }, 
			    { "Solvay", "Solvay" }, { "Somers", "Somers" }, { "Sonyea", "Sonyea" }, { "Sound Beach", "Sound Beach" }, { "South Bethlehem", "South Bethlehem" },
			    { "South Butler", "South Butler" }, { "South Byron", "South Byron" }, { "South Cairo", "South Cairo" }, { "South Cheektowaga", "South Cheektowaga" }, 
			    { "South Colton", "South Colton" }, { "South Corning", "South Corning" }, { "South Dayton", "South Dayton" }, { "South Edmeston", "South Edmeston" },
			    { "South Fallsburg", "South Fallsburg" }, { "South Farmingdale", "South Farmingdale" }, { "South Floral Park", "South Floral Park" }, 
			    { "South Glens Falls", "South Glens Falls" }, { "South Hempstead", "South Hempstead" }, { "South Huntington", "South Huntington" }, 
			    { "South Jamesport", "South Jamesport" }, { "South Kortright", "South Kortright" }, { "South Lima", "South Lima" }, 
			    { "South New Berlin", "South New Berlin" }, { "South Otselic", "South Otselic" }, { "South Ozone Park", "South Ozone Park" }, 
			    { "South Plymouth", "South Plymouth" }, { "South Richmond Hill", "South Richmond Hill" }, { "South Rutland", "South Rutland" }, { "South Salem", "South Salem" },
			    { "South Schodack", "South Schodack" }, { "South Setauket", "South Setauket" }, { "South Wales", "South Wales" }, { "South Westerlo", "South Westerlo" },
			    { "Southampton", "Southampton" }, { "Southfields", "Southfields" }, { "Southold", "Southold" }, { "Sparkill", "Sparkill" }, { "Sparrow Bush", "Sparrow Bush" },
			    { "Sparrowbush", "Sparrowbush" }, { "Speculator", "Speculator" }, { "Spencer", "Spencer" }, { "Spencerport", "Spencerport" }, { "Spencertown", "Spencertown" },
			    { "Speonk", "Speonk" }, { "Sprakers", "Sprakers" }, { "Spring Brook", "Spring Brook" }, { "Spring Glen", "Spring Glen" }, { "Spring Valley", "Spring Valley" },
			    { "Springfield Center", "Springfield Center" }, { "Springfield Gardens", "Springfield Gardens" }, { "Springville", "Springville" }, 
			    { "Springwater", "Springwater" }, { "Staatsburg", "Staatsburg" }, { "Stafford", "Stafford" }, { "Stamford", "Stamford" }, 
			    { "Stanfordville", "Stanfordville" }, { "Stanley", "Stanley" }, { "Star Lake", "Star Lake" }, { "Staten Island", "Staten Island" }, 
			    { "Steamburg", "Steamburg" }, { "Stella Niagara", "Stella Niagara" }, { "Stephentown", "Stephentown" }, { "Sterling", "Sterling" },
			    { "Sterling Forest", "Sterling Forest" }, { "Stewart Manor", "Stewart Manor" }, { "Stillwater", "Stillwater" }, { "Stittville", "Stittville" },
			    { "Stockton", "Stockton" }, { "Stone Ridge", "Stone Ridge" }, { "Stony Brook", "Stony Brook" }, { "Stony Creek", "Stony Creek" },
			    { "Stony Point", "Stony Point" }, { "Stormville", "Stormville" }, { "Stottville", "Stottville" }, { "Stow", "Stow" }, { "Stratford", "Stratford" }, 
			    { "Strykersville", "Strykersville" }, { "Stuyvesant", "Stuyvesant" }, { "Stuyvesant Falls", "Stuyvesant Falls" }, { "Suffern", "Suffern" },
			    { "Sugar Loaf", "Sugar Loaf" }, { "Summit", "Summit" }, { "Summitville", "Summitville" }, { "Sundown", "Sundown" }, { "Sunnyside", "Sunnyside" },
			    { "Surprise", "Surprise" }, { "Swain", "Swain" }, { "Swan Lake", "Swan Lake" }, { "Swormville", "Swormville" }, { "Sylvan Beach", "Sylvan Beach" },
			    { "Syosset", "Syosset" }, { "Syracuse", "Syracuse" }, { "Taberg", "Taberg" }, { "Taconic Lake", "Taconic Lake" }, { "Taghkanic", "Taghkanic" },
			    { "Tahawus", "Tahawus" }, { "Tallman", "Tallman" }, { "Tannersville", "Tannersville" }, { "Tappan", "Tappan" }, { "Tarrytown", "Tarrytown" },
			    { "Thendara", "Thendara" }, { "Theresa", "Theresa" }, { "Thiells", "Thiells" }, { "Thompson Ridge", "Thompson Ridge" }, { "Thompsonville", "Thompsonville" },
			    { "Thomson", "Thomson" }, { "Thornwood", "Thornwood" }, { "Thousand Island Park", "Thousand Island Park" }, { "Three Mile Bay", "Three Mile Bay" },
			    { "Thurman", "Thurman" }, { "Ticonderoga", "Ticonderoga" }, { "Tillson", "Tillson" }, { "Tioga Center", "Tioga Center" }, { "Tivoli", "Tivoli" },
			    { "Tomkins Cove", "Tomkins Cove" }, { "Tonawanda", "Tonawanda" }, { "Town Of Tonawanda", "Town Of Tonawanda" }, { "Treadwell", "Treadwell" },
			    { "Tribes Hill", "Tribes Hill" }, { "Troupsburg", "Troupsburg" }, { "Trout Creek", "Trout Creek" }, { "Troy", "Troy" }, { "Trumansburg", "Trumansburg" },
			    { "Truxton", "Truxton" }, { "Tuckahoe", "Tuckahoe" }, { "Tully", "Tully" }, { "Tunnel", "Tunnel" }, { "Tupper Lake", "Tupper Lake" }, 
			    { "Turin", "Turin" }, { "Tuscarora", "Tuscarora" }, { "Tuxedo Park", "Tuxedo Park" }, { "Tyrone", "Tyrone" }, { "Ulster Park", "Ulster Park" },
			    { "Unadilla", "Unadilla" }, { "Union Hill", "Union Hill" }, { "Union Springs", "Union Springs" }, { "Uniondale", "Uniondale" }, 
			    { "Unionville", "Unionville" }, { "Upper Jay", "Upper Jay" }, { "Upper Saint Regis", "Upper Saint Regis" }, { "Upton", "Upton" }, 
			    { "Utica", "Utica" }, { "Vails Gate", "Vails Gate" }, { "Valatie", "Valatie" }, { "Valhalla", "Valhalla" }, { "Valley Cottage", "Valley Cottage" },
			    { "Valley Falls", "Valley Falls" }, { "Valley Stream", "Valley Stream" }, { "Valois", "Valois" }, { "Van Buren Bay", "Van Buren Bay" },
			    { "Van Buren Point", "Van Buren Point" }, { "Van Etten", "Van Etten" }, { "Van Hornesville", "Van Hornesville" }, { "Varysburg", "Varysburg" },
			    { "Venice Center", "Venice Center" }, { "Verbank", "Verbank" }, { "Vermontville", "Vermontville" }, { "Vernon", "Vernon" }, { "Vernon Center",
			    "Vernon Center" }, { "Verona", "Verona" }, { "Verona Beach", "Verona Beach" }, { "Verplanck", "Verplanck" }, { "Versailles", "Versailles" },
			    { "Vestal", "Vestal" }, { "Veterans Administration", "Veterans Administration" }, { "Victor", "Victor" }, { "Victory Mills", "Victory Mills" },
			    { "Village Of Garden City", "Village Of Garden City" }, { "Voorheesville", "Voorheesville" }, { "Waccabuc", "Waccabuc" }, { "Waddington", "Waddington" }, 
			    { "Wadhams", "Wadhams" }, { "Wading River", "Wading River" }, { "Wadsworth", "Wadsworth" }, { "Wainscott", "Wainscott" }, { "Walden", "Walden" },
			    { "Wales Center", "Wales Center" }, { "Walker Valley", "Walker Valley" }, { "Wallace", "Wallace" }, { "Wallkill", "Wallkill" }, { "Walton", "Walton" },
			    { "Walworth", "Walworth" }, { "Wampsville", "Wampsville" }, { "Wanakena", "Wanakena" }, { "Wantagh", "Wantagh" }, { "Wappingers Falls", "Wappingers Falls" },
			    { "Warners", "Warners" }, { "Warnerville", "Warnerville" }, { "Warrensburg", "Warrensburg" }, { "Warsaw", "Warsaw" }, { "Warwick", "Warwick" }, 
			    { "Washington Mills", "Washington Mills" }, { "Washingtonville", "Washingtonville" }, { "Wassaic", "Wassaic" }, { "Water Mill", "Water Mill" }, 
			    { "Waterford", "Waterford" }, { "Waterloo", "Waterloo" }, { "Waterport", "Waterport" }, { "Watertown", "Watertown" }, { "Waterville", "Waterville" },
			    { "Watervliet", "Watervliet" }, { "Watkins Glen", "Watkins Glen" }, { "Wave Crest", "Wave Crest" }, { "Waverly", "Waverly" }, { "Wawarsing", "Wawarsing" },
			    { "Wayland", "Wayland" }, { "Wayne", "Wayne" }, { "Webster", "Webster" }, { "Webster Crossing", "Webster Crossing" }, { "Weedsport", "Weedsport" }, 
			    { "Wellesley Island", "Wellesley Island" }, { "Wells", "Wells" }, { "Wells Bridge", "Wells Bridge" }, { "Wellsburg", "Wellsburg" }, 
			    { "Wellsville", "Wellsville" }, { "West Amherst", "West Amherst" }, { "West Babylon", "West Babylon" }, { "West Bangor", "West Bangor" }, 
			    { "West Bloomfield", "West Bloomfield" }, { "West Brentwood", "West Brentwood" }, { "West Burlington", "West Burlington" }, { "West Camp", "West Camp" },
			    { "West Charlton", "West Charlton" }, { "West Chazy", "West Chazy" }, { "West Clarksville", "West Clarksville" }, { "West Coxsackie", "West Coxsackie" },
			    { "West Danby", "West Danby" }, { "West Davenport", "West Davenport" }, { "West Eaton", "West Eaton" }, { "West Edmeston", "West Edmeston" }, 
			    { "West Ellicott", "West Ellicott" }, { "West Exeter", "West Exeter" }, { "West Falls", "West Falls" }, { "West Fishkill", "West Fishkill" }, 
			    { "West Fulton", "West Fulton" }, { "West Gilgo Beach", "West Gilgo Beach" }, { "West Harrison", "West Harrison" }, { "West Haverstraw", "West Haverstraw" },
			    { "West Hempstead", "West Hempstead" }, { "West Henrietta", "West Henrietta" }, { "West Hurley", "West Hurley" }, { "West Islip", "West Islip" }, 
			    { "West Kill", "West Kill" }, { "West Lebanon", "West Lebanon" }, { "West Leyden", "West Leyden" }, { "West Monroe", "West Monroe" },
			    { "West Nyack", "West Nyack" }, { "West Oneonta", "West Oneonta" }, { "West Park", "West Park" }, { "West Point", "West Point" }, 
			    { "West Rush", "West Rush" }, { "West Sand Lake", "West Sand Lake" }, { "West Sayville", "West Sayville" }, { "West Seneca", "West Seneca" },
			    { "West Shokan", "West Shokan" }, { "West Stockholm", "West Stockholm" }, { "West Valley", "West Valley" }, { "West Windsor", "West Windsor" },
			    { "West Winfield", "West Winfield" }, { "Westbrookville", "Westbrookville" }, { "Westbury", "Westbury" }, { "Westdale", "Westdale" }, 
			    { "Westerlo", "Westerlo" }, { "Westernville", "Westernville" }, { "Westfield", "Westfield" }, { "Westford", "Westford" }, { "Westgate", "Westgate" },
			    { "Westhampton", "Westhampton" }, { "Westhampton Beach", "Westhampton Beach" }, { "Westmoreland", "Westmoreland" }, { "Westons Mills", "Westons Mills" }, 
			    { "Westport", "Westport" }, { "Westtown", "Westtown" }, { "Wevertown", "Wevertown" }, { "Whallonsburg", "Whallonsburg" }, 
			    { "Wheatley Heights", "Wheatley Heights" }, { "Whippleville", "Whippleville" }, { "White Creek", "White Creek" }, { "White Lake", "White Lake" },
			    { "White Plains", "White Plains" }, { "White Sulphur Springs", "White Sulphur Springs" }, { "Whiteface Mountain", "Whiteface Mountain" },
			    { "Whitehall", "Whitehall" }, { "Whitesboro", "Whitesboro" }, { "Whitestone", "Whitestone" }, { "Whitesville", "Whitesville" },
			    { "Whitney Point", "Whitney Point" }, { "Wiccopee", "Wiccopee" }, { "Willard", "Willard" }, { "Willet", "Willet" }, { "Williamson", "Williamson" },
			    { "Williamstown", "Williamstown" }, { "Williamsville", "Williamsville" }, { "Williston Park", "Williston Park" }, { "Willow", "Willow" }, 
			    { "Willsboro", "Willsboro" }, { "Willseyville", "Willseyville" }, { "Wilmington", "Wilmington" }, { "Wilson", "Wilson" }, { "Wilton", "Wilton" },
			    { "Windham", "Windham" }, { "Windsor", "Windsor" }, { "Wingdale", "Wingdale" }, { "Winthrop", "Winthrop" }, { "Witherbee", "Witherbee" },
			    { "Wolcott", "Wolcott" }, { "Woodbourne", "Woodbourne" }, { "Woodbury", "Woodbury" }, { "Woodgate", "Woodgate" }, { "Woodhaven", "Woodhaven" },
			    { "Woodhull", "Woodhull" }, { "Woodmere", "Woodmere" }, { "Woodridge", "Woodridge" }, { "Woodside", "Woodside" }, { "Woodstock", "Woodstock" },
			    { "Woodville", "Woodville" }, { "Worcester", "Worcester" }, { "Wurtsboro", "Wurtsboro" }, { "Wyandanch", "Wyandanch" }, { "Wykagyl", "Wykagyl" },
			    { "Wynantskill", "Wynantskill" }, { "Wyoming", "Wyoming" }, { "Yaphank", "Yaphank" }, { "Yonkers", "Yonkers" }, { "York", "York" }, { "Yorkshire",
			    "Yorkshire" }, { "Yorktown Heights", "Yorktown Heights" }, { "Yorkville", "Yorkville" }, { "Youngstown", "Youngstown" }, { "Youngsville", "Youngsville" },
			    { "Yulan", "Yulan"}
			};

			return createEnumResponseValidator(keyValues);
	}
	
	private ResponseSpecification getSchoolByCityStateResponseValidator()
	
	{ 
		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("creditCardState[0]",equalTo("NY"))
		.expectBody("zipcode[0]",equalTo("12007"))
		.expectBody("schoolStatus[0]",equalTo("1"))
		.expectBody("groupType[0]",equalTo("EC_DR"))
		.expectBody("address1[0]",equalTo("45 ALCOVE RD"))
		.expectBody("city[0]",equalTo("ALCOVE"))
		.expectBody("schoolUCN[0]",equalTo("600389789"))
		.expectBody("name[0]",equalTo("FUN AND FRIENDS PRESCHOOL"))
		.expectBody("state[0]",equalTo("NY"))
		.expectBody("spsId[0]",equalTo("11943489"));
		return rspec.build();
	}
	
	private ResponseSpecification getSchoolByZipcodeResponseValidator()
	
	{
		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("creditCardState[0]",equalTo("NY"))
		.expectBody("zipcode[0]",equalTo("11431"))
		.expectBody("schoolStatus[0]",equalTo("1"))
		.expectBody("groupType[0]",equalTo("EC_PS"))
		.expectBody("address1[0]",equalTo("PO BOX 311215"))
		.expectBody("city[0]",equalTo("JAMAICA"))
		.expectBody("schoolUCN[0]",equalTo("610166776"))
		.expectBody("name[0]",equalTo("EARLY SUNRISE PRE - SCH AND KN"))
		.expectBody("state[0]",equalTo("NY"))
		.expectBody("spsId[0]",equalTo("30097347"))

		.expectBody("creditCardState[1]",equalTo("NY"))
		.expectBody("zipcode[1]",equalTo("11431"))
		.expectBody("schoolStatus[1]",equalTo("1"))
		.expectBody("groupType[1]",equalTo("EC_PS"))
		.expectBody("address1[1]",equalTo("PO BOX 311215"))
		.expectBody("city[1]",equalTo("JAMAICA"))
		.expectBody("schoolUCN[1]",equalTo("610166776"))
		.expectBody("name[1]",equalTo("EARLY SUNRISE PRE-SCHOOL AND KINDERGARTEN II"))
		.expectBody("state[1]",equalTo("NY"))
		.expectBody("spsId[1]",equalTo("30160821"));
		return rspec.build();
	}
	
		private ResponseSpecification getOrgInfoResponseValidator()
	
	{ 
		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("zipcode",equalTo("11372"))
		.expectBody("reportingSchoolType",equalTo("1"))
		.expectBody("groupType",equalTo("S_MA"))
		.expectBody("country",equalTo("US"))
		.expectBody("creditCardState",equalTo("NY"))
		.expectBody("poBox",equalTo("0"))
		.expectBody("schoolStatus",equalTo("0"))
		.expectBody("addressTypeCode",equalTo("School"))
		.expectBody("address1",equalTo("34-25 82ND STREET"))
		.expectBody("city",equalTo("JACKSON HEIGHTS"))
		.expectBody("createDate",equalTo("2003-05-22 20:38:19.729153"))
		.expectBody("createdSource",equalTo("DMGR"))
		.expectBody("name",equalTo("P.S. 212"))
		.expectBody("state",equalTo("NY"))
		.expectBody("spsId",equalTo("264321"));
		return rspec.build();
	}
	
		private ResponseSpecification getCityStateByZipCodeResponseValidator()
	
	{ 
		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("cities",hasItems("Flushing", "Jackson Heights"))
		.expectBody("state",equalTo("NY"));
		return rspec.build();
	}
		
		private ResponseSpecification getSchoolDistrictParentOrgsResponseValidator()
		
		{
			ResponseSpecBuilder rspec=new ResponseSpecBuilder()
			
			.expectBody("addressType[0]",equalTo("05"))
			.expectBody("borderId[0]",equalTo("03865701"))
			.expectBody("city[0]",equalTo("PLAISTOW"))
			.expectBody("country[0]",equalTo("US"))
			.expectBody("domesticCode[0]",equalTo("01"))		
			.expectBody("groupTypeCode[0]",equalTo("D_SN"))
			.expectBody("groupTypeCodeLbl[0]",equalTo("SUPERVISORY UNION (SCH)"))
			.expectBody("id[0]",equalTo("11883975"))
			.expectBody("name[0]",equalTo("TIMBERLANE REGIONAL SCH DIST"))
			.expectBody("phoneExtension[0]",equalTo("0"))
			.expectBody("phoneNumber[0]",equalTo("6033826119"))
			.expectBody("publicPrivateKey[0]",equalTo("01"))
			.expectBody("state[0]",equalTo("NH"))
			.expectBody("status[0]",equalTo("true"))
			.expectBody("ucn[0]",equalTo("600009037"))
			.expectBody("uspsType[0]",equalTo("2"))
			.expectBody("zip[0]",equalTo("03865-2724"))
			.expectBody("address1[0]",equalTo("30 GREENOUGH RD"))
			.expectBody("address2[0]",isEmptyString())
			.expectBody("address3[0]",isEmptyString())
			.expectBody("address4[0]",isEmptyString())
			.expectBody("address5[0]",isEmptyString())
			.expectBody("county[0]",isEmptyString())
			.expectBody("createDate[0]",isEmptyString())
			.expectBody("createSource[0]",isEmptyString())
			.expectBody("lastModifiedDate[0]",isEmptyString())
			.expectBody("poBox[0]",isEmptyString())
			.expectBody("reportingType[0]",isEmptyString())
			.expectBody("survivingUcn[0]",isEmptyString())
			
			.expectBody("address2[1]",isEmptyString())
			.expectBody("address3[1]",isEmptyString())
			.expectBody("address4[1]",isEmptyString())
			.expectBody("address5[1]",isEmptyString())
			.expectBody("county[1]",isEmptyString())
			.expectBody("createDate[1]",isEmptyString())
			.expectBody("createSource[1]",isEmptyString())
			.expectBody("lastModifiedDate[1]",isEmptyString())
			.expectBody("poBox[1]",isEmptyString())
			.expectBody("reportingType[1]",isEmptyString())
			.expectBody("survivingUcn[1]",isEmptyString())
			.expectBody("addressType[1]",equalTo("05"))
			.expectBody("borderId[1]",equalTo("03865702"))
			.expectBody("city[1]",equalTo("PLAISTOW"))
			.expectBody("country[1]",equalTo("US"))
			.expectBody("domesticCode[1]",equalTo("01"))
			.expectBody("groupTypeCode[1]",equalTo("D_SU"))
			.expectBody("groupTypeCodeLbl[1]",equalTo("INTERMEDIATE DISTRICT (SCH)"))
			.expectBody("id[1]",equalTo("11883603"))
			.expectBody("name[1]",equalTo("SCHOOL ADMINISTRATIVE UNIT 55"))
			.expectBody("phoneExtension[1]",equalTo("0"))
			.expectBody("phoneNumber[1]",equalTo("6033826119"))
			.expectBody("publicPrivateKey[1]",equalTo("01"))
			.expectBody("state[1]",equalTo("NH"))
			.expectBody("status[1]",equalTo("true"))
			.expectBody("ucn[1]",equalTo("600009098"))
			.expectBody("uspsType[1]",equalTo("2"))
			.expectBody("zip[1]",equalTo("03865-2724"))
			.expectBody("address1[1]",equalTo("30 GREENOUGH RD"));
			return rspec.build();
		}
		
		
		private ResponseSpecification getSchoolDistrictTopParentOrgResponseValidator()
		
		{
			ResponseSpecBuilder rspec=new ResponseSpecBuilder()
			.expectBody("address2[0]",isEmptyString())
			.expectBody("address3[0]",isEmptyString())
			.expectBody("address4[0]",isEmptyString())
			.expectBody("address5[0]",isEmptyString())
			.expectBody("county[0]",isEmptyString())
			.expectBody("createDate[0]",isEmptyString())
			.expectBody("createSource[0]",isEmptyString())
			.expectBody("lastModifiedDate[0]",isEmptyString())
			.expectBody("poBox[0]",isEmptyString())
			.expectBody("reportingType[0]",isEmptyString())
			.expectBody("survivingUcn[0]",isEmptyString())
			.expectBody("addressType[0]",equalTo("05"))
			.expectBody("borderId[0]",equalTo("03865702"))
			.expectBody("city[0]",equalTo("PLAISTOW"))
			.expectBody("country[0]",equalTo("US"))
			.expectBody("domesticCode[0]",equalTo("01"))
			.expectBody("groupTypeCode[0]",equalTo("D_SU"))
			.expectBody("groupTypeCodeLbl[0]",equalTo("INTERMEDIATE DISTRICT (SCH)"))
			.expectBody("id[0]",equalTo("11883603"))
			.expectBody("name[0]",equalTo("SCHOOL ADMINISTRATIVE UNIT 55"))
			.expectBody("phoneExtension[0]",equalTo("0"))
			.expectBody("phoneNumber[0]",equalTo("6033826119"))
			.expectBody("publicPrivateKey[0]",equalTo("01"))
			.expectBody("state[0]",equalTo("NH"))
			.expectBody("status[0]",equalTo("true"))
			.expectBody("ucn[0]",equalTo("600009098"))
			.expectBody("uspsType[0]",equalTo("2"))
			.expectBody("zip[0]",equalTo("03865-2724"))
			.expectBody("address1[0]",equalTo("30 GREENOUGH RD"));
			return rspec.build();
		}
		
		
		private ResponseSpecification getSchoolDistrictSubOrgsResponseValidator()
		
		{
			ResponseSpecBuilder rspec=new ResponseSpecBuilder()
			.expectBody("address2[0]",isEmptyString())
			.expectBody("address3[0]",isEmptyString())
			.expectBody("address4[0]",isEmptyString())
			.expectBody("address5[0]",isEmptyString())
			.expectBody("county[0]",isEmptyString())
			.expectBody("createDate[0]",isEmptyString())
			.expectBody("createSource[0]",isEmptyString())
			.expectBody("lastModifiedDate[0]",isEmptyString())
			.expectBody("poBox[0]",isEmptyString())
			.expectBody("reportingType[0]",isEmptyString())
			.expectBody("survivingUcn[0]",isEmptyString())
			.expectBody("addressType[0]",equalTo("06"))
			.expectBody("borderId[0]",equalTo("01420040"))
			.expectBody("city[0]",equalTo("FITCHBURG"))
			.expectBody("country[0]",equalTo("US"))
			.expectBody("domesticCode[0]",equalTo("01"))
			.expectBody("groupTypeCode[0]",equalTo("S_MI"))
			.expectBody("groupTypeCodeLbl[0]",equalTo("MIDDLE SCHOOL"))
			.expectBody("id[0]",equalTo("11880687"))
			.expectBody("name[0]",equalTo("B F BROWN ART VISION SCHOOL"))
			.expectBody("phoneExtension[0]",equalTo("0"))
			.expectBody("phoneNumber[0]",equalTo("9783453279"))
			.expectBody("publicPrivateKey[0]",equalTo("01"))
			.expectBody("state[0]",equalTo("MA"))
			.expectBody("status[0]",equalTo("true"))
			.expectBody("ucn[0]",equalTo("600062062"))
			.expectBody("uspsType[0]",equalTo("2"))
			.expectBody("zip[0]",equalTo("01420"))
			.expectBody("address1[0]",equalTo("188 ELM ST"))
			
			.expectBody("address4[1]",isEmptyString())
			.expectBody("address5[1]",isEmptyString())
			.expectBody("county[1]",isEmptyString())
			.expectBody("createDate[1]",isEmptyString())
			.expectBody("createSource[1]",isEmptyString())
			.expectBody("lastModifiedDate[1]",isEmptyString())
			.expectBody("poBox[1]",isEmptyString())
			.expectBody("reportingType[1]",isEmptyString())
			.expectBody("survivingUcn[1]",isEmptyString())
			.expectBody("addressType[1]",equalTo("06"))
			.expectBody("borderId[1]",equalTo("01420055"))
			.expectBody("city[1]",equalTo("FITCHBURG"))
			.expectBody("country[1]",equalTo("US"))
			.expectBody("domesticCode[1]",equalTo("01"))
			.expectBody("groupTypeCode[1]",equalTo("S_6E"))
			.expectBody("groupTypeCodeLbl[1]",equalTo("ELEMENTARY SCHOOL"))
			.expectBody("id[1]",equalTo("11880908"))
			.expectBody("name[1]",equalTo("CROCKER ELEMENTARY SCHOOL"))
			.expectBody("phoneExtension[1]",equalTo("0"))
			.expectBody("phoneNumber[1]",equalTo("9783453290"))
			.expectBody("publicPrivateKey[1]",equalTo("01"))
			.expectBody("state[1]",equalTo("MA"))
			.expectBody("status[1]",equalTo("true"))
			.expectBody("ucn[1]",equalTo("600062063"))
			.expectBody("uspsType[1]",equalTo("2"))
			.expectBody("zip[1]",equalTo("01420"))
			.expectBody("address1[1]",equalTo("200 BIGELOW RD"))
			
			.expectBody("address4[2]",isEmptyString())
			.expectBody("address5[2]",isEmptyString())
			.expectBody("county[2]",isEmptyString())
			.expectBody("createDate[2]",isEmptyString())
			.expectBody("createSource[2]",isEmptyString())
			.expectBody("lastModifiedDate[2]",isEmptyString())
			.expectBody("poBox[2]",isEmptyString())
			.expectBody("reportingType[2]",isEmptyString())
			.expectBody("survivingUcn[2]",isEmptyString())
			.expectBody("addressType[2]",equalTo("06"))
			.expectBody("borderId[2]",equalTo("01420090"))
			.expectBody("city[2]",equalTo("FITCHBURG"))
			.expectBody("country[2]",equalTo("US"))
			.expectBody("domesticCode[2]",equalTo("01"))
			.expectBody("groupTypeCode[2]",equalTo("S_S"))
			.expectBody("groupTypeCodeLbl[2]",equalTo("SENIOR HIGH SCHOOL"))
			.expectBody("id[2]",equalTo("11881045"))
			.expectBody("name[2]",equalTo("FITCHBURG HIGH SCHOOL"))
			.expectBody("phoneExtension[2]",equalTo("100"))
			.expectBody("phoneNumber[2]",equalTo("9783453240"))
			.expectBody("publicPrivateKey[2]",equalTo("01"))
			.expectBody("state[2]",equalTo("MA"))
			.expectBody("status[2]",equalTo("true"))
			.expectBody("ucn[2]",equalTo("600062064"))
			.expectBody("uspsType[2]",equalTo("2"))
			.expectBody("zip[2]",equalTo("01420"))
			.expectBody("address1[2]",equalTo("140 ARNHOW FARM RD"))
			
			.expectBody("address4[3]",isEmptyString())
			.expectBody("address5[3]",isEmptyString())
			.expectBody("county[3]",isEmptyString())
			.expectBody("createDate[3]",isEmptyString())
			.expectBody("createSource[3]",isEmptyString())
			.expectBody("lastModifiedDate[3]",isEmptyString())
			.expectBody("poBox[3]",isEmptyString())
			.expectBody("reportingType[3]",isEmptyString())
			.expectBody("survivingUcn[3]",isEmptyString())
			.expectBody("addressType[3]",equalTo("06"))
			.expectBody("borderId[3]",equalTo("01420080"))
			.expectBody("city[3]",equalTo("FITCHBURG"))
			.expectBody("country[3]",equalTo("US"))
			.expectBody("domesticCode[3]",equalTo("01"))
			.expectBody("groupTypeCode[3]",equalTo("S_6E"))
			.expectBody("groupTypeCodeLbl[3]",equalTo("ELEMENTARY SCHOOL"))
			.expectBody("id[3]",equalTo("11881289"))
			.expectBody("name[3]",equalTo("MCKAY CAMPUS ELEMENTARY SCHOOL"))
			.expectBody("phoneExtension[3]",equalTo("0"))
			.expectBody("phoneNumber[3]",equalTo("9786653425"))
			.expectBody("publicPrivateKey[3]",equalTo("01"))
			.expectBody("state[3]",equalTo("MA"))
			.expectBody("status[3]",equalTo("true"))
			.expectBody("ucn[3]",equalTo("600062065"))
			.expectBody("uspsType[3]",equalTo("2"))
			.expectBody("zip[3]",equalTo("01420"))
			.expectBody("address1[3]",equalTo("67 RINDGE RD"))
			
			.expectBody("address4[4]",isEmptyString())
			.expectBody("address5[4]",isEmptyString())
			.expectBody("county[4]",isEmptyString())
			.expectBody("createDate[4]",isEmptyString())
			.expectBody("createSource[4]",isEmptyString())
			.expectBody("lastModifiedDate[4]",isEmptyString())
			.expectBody("poBox[4]",isEmptyString())
			.expectBody("reportingType[4]",isEmptyString())
			.expectBody("survivingUcn[4]",isEmptyString())
			.expectBody("addressType[4]",equalTo("06"))
			.expectBody("borderId[4]",equalTo("01420195"))
			.expectBody("city[4]",equalTo("FITCHBURG"))
			.expectBody("country[4]",equalTo("US"))
			.expectBody("domesticCode[4]",equalTo("01"))
			.expectBody("groupTypeCode[4]",equalTo("S_6E"))
			.expectBody("groupTypeCodeLbl[4]",equalTo("ELEMENTARY SCHOOL"))
			.expectBody("id[4]",equalTo("11881552"))
			.expectBody("name[4]",equalTo("MEMORIAL MIDDLE SCHOOL"))
			.expectBody("phoneExtension[4]",equalTo("0"))
			.expectBody("phoneNumber[4]",equalTo("9783453295"))
			.expectBody("publicPrivateKey[4]",equalTo("01"))
			.expectBody("state[4]",equalTo("MA"))
			.expectBody("status[4]",equalTo("true"))
			.expectBody("ucn[4]",equalTo("600062066"))
			.expectBody("uspsType[4]",equalTo("2"))
			.expectBody("zip[4]",equalTo("01420"))
			.expectBody("address1[4]",equalTo("615 ROLLSTONE ST"))

			.expectBody("address4[5]",isEmptyString())
			.expectBody("address5[5]",isEmptyString())
			.expectBody("county[5]",isEmptyString())
			.expectBody("createDate[5]",isEmptyString())
			.expectBody("createSource[5]",isEmptyString())
			.expectBody("lastModifiedDate[5]",isEmptyString())
			.expectBody("poBox[5]",isEmptyString())
			.expectBody("reportingType[5]",isEmptyString())
			.expectBody("survivingUcn[5]",isEmptyString())
			.expectBody("addressType[5]",equalTo("06"))
			.expectBody("borderId[5]",equalTo("01420215"))
			.expectBody("city[5]",equalTo("FITCHBURG"))
			.expectBody("country[5]",equalTo("US"))
			.expectBody("domesticCode[5]",equalTo("01"))
			.expectBody("groupTypeCode[5]",equalTo("S_6E"))
			.expectBody("groupTypeCodeLbl[5]",equalTo("ELEMENTARY SCHOOL"))
			.expectBody("id[5]",equalTo("11881716"))
			.expectBody("name[5]",equalTo("REINGOLD ELEMENTARY SCHOOL"))
			.expectBody("phoneExtension[5]",equalTo("0"))
			.expectBody("phoneNumber[5]",equalTo("9783453287"))
			.expectBody("publicPrivateKey[5]",equalTo("01"))
			.expectBody("state[5]",equalTo("MA"))
			.expectBody("status[5]",equalTo("true"))
			.expectBody("ucn[5]",equalTo("600062067"))
			.expectBody("uspsType[5]",equalTo("2"))
			.expectBody("zip[5]",equalTo("01420"))
			.expectBody("address1[5]",equalTo("70 REINGOLD AVE"))
			
			.expectBody("address4[6]",isEmptyString())
			.expectBody("address5[6]",isEmptyString())
			.expectBody("county[6]",isEmptyString())
			.expectBody("createDate[6]",isEmptyString())
			.expectBody("createSource[6]",isEmptyString())
			.expectBody("lastModifiedDate[6]",isEmptyString())
			.expectBody("poBox[6]",isEmptyString())
			.expectBody("reportingType[6]",isEmptyString())
			.expectBody("survivingUcn[6]",isEmptyString())
			.expectBody("addressType[6]",equalTo("06"))
			.expectBody("borderId[6]",equalTo("01420240"))
			.expectBody("city[6]",equalTo("FITCHBURG"))
			.expectBody("country[6]",equalTo("US"))
			.expectBody("domesticCode[6]",equalTo("01"))
			.expectBody("groupTypeCode[6]",equalTo("S_6E"))
			.expectBody("groupTypeCodeLbl[6]",equalTo("ELEMENTARY SCHOOL"))
			.expectBody("id[6]",equalTo("11889972"))
			.expectBody("name[6]",equalTo("SOUTH STREET ELEMENTARY SCHOOL"))
			.expectBody("phoneExtension[6]",equalTo("0"))
			.expectBody("phoneNumber[6]",equalTo("9783482300"))
			.expectBody("publicPrivateKey[6]",equalTo("01"))
			.expectBody("state[6]",equalTo("MA"))
			.expectBody("status[6]",equalTo("true"))
			.expectBody("ucn[6]",equalTo("600121206"))
			.expectBody("uspsType[6]",equalTo("2"))
			.expectBody("zip[6]",equalTo("01420"))
			.expectBody("address1[6]",equalTo("376 SOUTH ST"))
			
			.expectBody("address4[7]",isEmptyString())
			.expectBody("address5[7]",isEmptyString())
			.expectBody("county[7]",isEmptyString())
			.expectBody("createDate[7]",isEmptyString())
			.expectBody("createSource[7]",isEmptyString())
			.expectBody("lastModifiedDate[7]",isEmptyString())
			.expectBody("poBox[7]",isEmptyString())
			.expectBody("reportingType[7]",isEmptyString())
			.expectBody("survivingUcn[7]",isEmptyString())
			.expectBody("addressType[7]",equalTo("05"))
			.expectBody("borderId[7]",equalTo("01420017"))
			.expectBody("city[7]",equalTo("FITCHBURG"))
			.expectBody("country[7]",equalTo("US"))
			.expectBody("domesticCode[7]",equalTo("01"))
			.expectBody("groupTypeCode[7]",equalTo("OS_AL"))
			.expectBody("groupTypeCodeLbl[7]",equalTo("ALTERNATIVE CONTINUING ED"))
			.expectBody("id[7]",equalTo("11903520"))
			.expectBody("name[7]",equalTo("CALDWELL ALT SCHOOL"))
			.expectBody("phoneExtension[7]",equalTo("0"))
			.expectBody("phoneNumber[7]",equalTo("9783455250"))
			.expectBody("publicPrivateKey[7]",equalTo("01"))
			.expectBody("state[7]",equalTo("MA"))
			.expectBody("status[7]",equalTo("true"))
			.expectBody("ucn[7]",equalTo("600211464"))
			.expectBody("uspsType[7]",equalTo("2"))
			.expectBody("zip[7]",equalTo("01420-7202"))
			.expectBody("address1[7]",equalTo("44 WANOOSNOC RD"));
			return rspec.build();
		}

		private Map<String, Object> createLogInPayload()

	{
		Map<String, Object> createLogInInfo = new HashMap<String, Object>();
		createLogInInfo.put("user",user );
		createLogInInfo.put("password",password);
		createLogInInfo.put("singleToken",singleToken);
		createLogInInfo.put("userForWS", userForWS);
		createLogInInfo.put("addCookie",addCookie);
		createLogInInfo.put("addExpiration",addExpiration);
		return createLogInInfo;
	
	}
	private static void addKeyValueValidations(ResponseSpecBuilder specBuilder,	String[][] keyValues) 
	{
		specBuilder.expectBody("size()", equalTo(keyValues.length));
		for (int i = 0; i < keyValues.length; i++) 
		{
			specBuilder.expectBody("get(" + i + ").key",
			equalTo(keyValues[i][0]));
			specBuilder.expectBody("get(" + i + ").name",
			equalTo(keyValues[i][1]));
		}
	}
	
	private ResponseSpecification createEnumResponseValidator(String[][] keyvalues )
	{
		ResponseSpecBuilder respec=new ResponseSpecBuilder()
			.expectStatusCode(200)
			.expectContentType(ContentType.JSON);
			addKeyValueValidations(respec,keyvalues);
			return respec.build();
	}
	
}
